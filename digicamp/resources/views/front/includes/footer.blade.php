<div class="footer-wrapper style-10">
    <footer class="type-1">
        <div class="footer-columns-entry">
            <div class="row">
                <div class="col-md-6">
                    <div class="col-md-6">
                        <a href="{{url('/')}}"><img src="{{asset('components/both/images/web')}}/Relationship Solution.png" alt="{{$web_name}}" class="img-footer" /></a>
                    </div>
                    <div class="col-md-6">
                        <a href="{{url('/')}}"><img src="{{asset('components/both/images/web')}}/Customer Care.png" alt="{{$web_name}}" class="img-footer" /></a>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="socials-box">
                            @foreach($social_media as $sm)
                                <a href="{{url($sm->value)}}">
                                    <img src="{{asset('components/both/images/web')}}/{{$sm->icon}}"></img>
                                </a>
                            @endforeach
                        </div>
                    </div>
                     <div class="copyright">  
                        <div class="col-md-12">
                            Copyright {{date('Y')}}, Wahana<br>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 solveway-dev">
                    <a href="{{url('http://solveway.co.id/')}}">Designed and Developed by PT. Solveway Dashindo Teknologi</a>
                </div>
            </div>
        </div>
    </footer>
</div>

<script type="text/javascript" src="{{asset('components/plugins/jquery.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/idangerous.swiper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/plugins/swiper-slider/swiper.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/global.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.mousewheel.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/jquery.jscrollpane.min.js')}}"></script>
<script src="{{asset('components/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js')}}"></script>
<script type="text/javascript" src="{{asset('components/front/js/kyubi-head.js')}}"></script>

@stack('custom_scripts')
