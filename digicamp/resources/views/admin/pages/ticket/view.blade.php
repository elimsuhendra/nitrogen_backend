@extends($view_path.'.layouts.master')
@push('css')
  <link href="{{asset('components/back/css/pages/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="profile">
    <div class="tabbable-line tabbable-full-width">
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_1_1" data-toggle="tab"> Overview </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1_1">
                <div class="row">
                    <div class="col-md-3">
                        <ul class="list-unstyled profile-nav">
                            <li>
                                @if($ticket->image != null && file_exists('components/both/images/movie/'.$ticket->image))
                                    <img src="{{asset('components/both/images/movie/'.$ticket->image)}}" class="img-responsive pic-bordered" alt="" />
                                @else
                                    <img src="{{asset('components/admin/image/default.jpg')}}" class="img-responsive pic-bordered" alt="" />
                                @endif
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="row">
                            <div class="col-md-8 profile-info">
                                <h4 class="font-green sbold">{{$ticket->ticket_name}}</h4>
                                <p>{{$ticket->synopsis}}</p>
                            </div>
                            <!--end col-md-8-->
                            <div class="col-md-4">
                                
                            </div>
                            <!--end col-md-4-->
                        </div>
                        <!--end row-->
                        <div class="tabbable-line tabbable-custom-profile">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_11" data-toggle="tab"> Distribution </a>
                                </li>
                                <!-- <li>
                                    <a href="#tab_1_12" data-toggle="tab"> Aktivitas </a>
                                </li> -->
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_11">
                                    <div class="portlet-body">
                                        @if(count($cinema_tickets) > 0)
                                        {{ $cinema_tickets->links() }}    
                                        <div class="table-scrollable">
                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                <thead>
                                                    <tr>
                                                        <tr>
                                                          <th>
                                                              <i class="fa fa-bell-o"></i> Cinema </th>
                                                          <th>
                                                              <i class="fa fa-building"></i> Type </th>
                                                          <th>
                                                              <i class="fa fa-building"></i> Showtime </th>
                                                          <th>
                                                              <i class="fa fa-map-marker"></i> Stock </th>
                                                          <th>
                                                              <i class="fa fa-clock-o"></i> Hold </th>
                                                            <th>
                                                                <i class="fa fa-compass"></i> Sold </th>
                                                      </tr>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                  @foreach($cinema_tickets as $ct)
                                                    <tr>
                                                        <td>
                                                          {{$ct->cinema->bioskop_name}}
                                                        </td>
                                                        <td>
                                                          {{$ct->cinema_service->name}}
                                                        </td>
                                                        <td>
                                                          {{date( 'd M Y', strtotime($ct->cinema_date) )}} {{$ct->cinema_time}}
                                                        </td>
                                                        <td> 
                                                          {{number_format($ct->stock)}}
                                                        </td>
                                                        <td>
                                                          {{number_format($ct->hold)}} 
                                                        </td>
                                                        <td>
                                                          {{number_format($ct->sold)}}  
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        @else
                                          <div class="alert alert-warning">No Data</div>
                                        @endif
                                    </div>
                                </div>

                                <!-- <div class="tab-pane" id="tab_1_12">
                                    <div class="portlet-body">
                                      
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end tab-pane-->
        </div>
    </div>
</div>
@endsection
@push('custom_scripts')

@endpush