@extends($view_path.'.layouts.master')
@push('css')
    <link href="{{asset('components/back/css/pages/profile-2.min.css')}}" rel="stylesheet" type="text/css" />
@endpush
@section('content')
<div class="profile">
    <div class="portlet light bordered">
        <div class="portlet-title">
          <div class="caption font-green">
            <i class="icon-layers font-green title-icon"></i>
            <span class="caption-subject bold uppercase"> {{$title}}</span>
          </div>
          <div class="actions">
            <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
          </div>
        </div>

        <div class="tabbable-line tabbable-full-width">
            <ul class="nav nav-tabs" id="tb_cus">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab"> Detail Post </a>
                </li>

                @if($order_hd != NULL)
                <li>
                    <a href="#tab_2_1" data-toggle="tab"> Detail Order </a>
                </li>
                @endif
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab_1_1">
                     <div class="portlet-body form">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                          <i class="fa fa-cogs"></i>Post Information 
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Title </div>
                                            <div class="col-md-7 value"> {{ $order_post->title ? $order_post->title : ' - ' }} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Description </div>
                                            <div class="col-md-7 value"> {{ $order_post->description ? $order_post->description : ' - ' }} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Tag  </div>
                                            <div class="col-md-7 value"> 
                                                @php
                                                  $tag = json_decode($order_post->tag_name);
                                                @endphp

                                                @if(count($tag) > 0)
                                                <ol class="cus_ol">
                                                    @foreach($tag as $q => $tg)
                                                      <li>{{ $q+1 }}. {{ $tg }}</li>
                                                    @endforeach
                                                </ol>
                                                @else
                                                -
                                                @endif
                                            </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Status </div>
                                            <div class="col-md-7 value"> 
                                                @if($order_post->status == 24)
                                                    <span class="label label-success">
                                                @elseif($order_post->status == 31)
                                                    <span class="label label-danger">
                                                @else
                                                    <span class="label label-info">
                                                @endif
                                                {{$order_post->desc}}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Name Customer </div>
                                            <div class="col-md-7 value"> {{ $order_post->name ? $order_post->name : ' - ' }} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Post Date  </div>
                                            <div class="col-md-7 value"> 
                                               @php 
                                                  $date = date_create($order_post->post_date);
                                              @endphp

                                              {{ date_format($date,"d - m - Y") }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="portlet yellow-crusta box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                          <i class="fa fa-cogs"></i>Event 
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Nama Event </div>
                                            <div class="col-md-7 value"> {{ $order_post->event ? $order_post->event : ' - ' }} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Event Date  </div>
                                            <div class="col-md-7 value"> 
                                               @php 
                                                  $date = date_create($order_post->event_date);
                                              @endphp

                                              {{ date_format($date,"d - m - Y") }}
                                            </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Province </div>
                                            <div class="col-md-7 value"> {{ $order_post->prov_name ? $order_post->prov_name : ' - ' }} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> City </div>
                                            <div class="col-md-7 value"> {{ $order_post->city ? $order_post->city : ' - ' }} </div>
                                        </div>

                                         <div class="row static-info">
                                            <div class="col-md-5 name"> Address </div>
                                            <div class="col-md-7 value"> {{ $order_post->address ? $order_post->address : ' - ' }} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Price </div>
                                            <div class="col-md-7 value"> Rp. {{ number_format($order_post->estimate_price) }} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                              <br><h3 class="sbold">Mitra Bid</h3>
                            </div>
                            
                            @if($mitra != NULL)
                            <div class="col-md-10">
                              <table class="table table-striped table-bordered table-advance table-hover">
                                <thead>
                                    <tr>
                                        <th><b>No</b></th>
                                        <th><b>Nama Mitra</b></th>
                                        <th><b>Bid Price</b></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($mitra as $q => $dt2)
                                        <tr>
                                          <td>{{ $q+1 }}</td>
                                          <td>{{ $dt2["name"] }}</td> 
                                          <td>Rp. {{ number_format($dt2["price"]) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              </table>
                            </div>
                            @else
                            <div class="col-md-10">
                                <h5>Belum Ada Penawaran</h5>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>

                @if($order_hd != NULL)
                <div class="tab-pane" id="tab_2_1">
                    <div class="portlet-body form">
                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="portlet yellow-crusta box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                          <i class="fa fa-cogs"></i>Order Details 
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Order #: </div>
                                            <div class="col-md-7 value"> {{$order_hd->transaction_id}}</div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Order Date & Time: </div>
                                            <div class="col-md-7 value"> {{date_format(date_create($order_hd->order_date),'d-m-Y H:i:s')}} </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Order Status: </div>
                                            <div class="col-md-7 value">
                                                @if($order_hd->sts_id == 6)
                                                    <span class="label label-success">
                                                @elseif($order_hd->sts_id == 11)
                                                    <span class="label label-danger">
                                                @else
                                                    <span class="label label-info">
                                                @endif
                                                {{$order_hd->desc}}
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Total: </div>
                                            <div class="col-md-7 value"> Rp. {{number_format($order_hd->total_price,0,',','.')}} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Customer Information
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Customer Name: </div>
                                            <div class="col-md-7 value"> {{$order_address->name}} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Email: </div>
                                            <div class="col-md-7 value"> {{$order_address->email}} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Phone: </div>
                                            <div class="col-md-7 value"> {{$order_address->phone}} </div>
                                        </div>

                                        <div class="row static-info">
                                            <div class="col-md-5 name"> Address: </div>
                                            <div class="col-md-7 value"> {{$order_address->address}} </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-cogs"></i>Service Detail
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-hover table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th> No </th>
                                                        <th> Mitra Name </th>
                                                        <th> Service Name </th>
                                                        <th> Price </th>
                                                        <th> Quantity </th>
                                                        <th> Status</th>
                                                        <th> Total Price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($order_dt as $q => $dt)
                                                        <tr>
                                                            <td> {{ $q+1 }}</td> 
                                                            <td> <b>{{$dt->mt_name}}</b></td>
                                                            <td> {{$dt->service_name}}</td>
                                                            <td> Rp. {{number_format($dt->price,0,',','.')}}</td>
                                                            <td> {{$dt->qty}}</td>
                                                            <td> 
                                                                @if($dt->ord_id == 6)
                                                                    <span class="label label-success">
                                                                @elseif($dt->ord_id == 11)
                                                                    <span class="label label-danger">
                                                                @else
                                                                    <span class="label label-info">
                                                                @endif
                                                                {{$dt->desc}}
                                                                </span>
                                                            </td>
                                                            <td> IDR {{number_format($dt->total_price,0,',','.')}} </td>
                                                        </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td colspan="6" class="text-right"><b>Grand Total</b></td>
                                                        <td class="text-right"><b>{{number_format($order_hd->total_price,0,',','.')}}</b></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        <!--end tab-pane-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
