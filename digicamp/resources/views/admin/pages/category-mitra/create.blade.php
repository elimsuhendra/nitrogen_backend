@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>

</style>

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'category_name','label' => 'Category Name','value' => (old('bioskop_name') ? old('bioskop_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'bioskop_name'])!!}

          <div class="form-group form-md-line-input col-md-12">
              <label>Category Image</label><br>
              <label class="btn green input-file-label-image_card">
                <input type="file" class="form-control col-md-12 single-image" name="image"> Pilih File
              </label>
                  <button type="button" class="btn red-mint remove-single-image" data-id="single-image" data-name="image">Hapus</button>
                <input type="hidden" name="remove-single-image-image" value="n">
                <br>
              <small>Note: File Must jpeg,png,jpg,gif | Max file size: 2Mb | Best Resolution: x px</small>

              <div class="form-group single-image-image col-md-12">
                <img src="{{asset($image_path2)}}/none.png" class="img-responsive thumbnail single-image-thumbnail">
              </div>
          </div>

        <div class="col-md-12 actions">
          {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
        </div>
      </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    // $(document).ready(function(){
        $('.title').keyup(function(){ 
          var slug = convertToSlug($(this).val());
          console.log(slug);
          $(".slug").val(slug);    
        });

        function convertToSlug(Text)
        {
            return Text
                .toLowerCase()
                .replace(/ /g,'-')
                .replace(/[^\w-]+/g,'')
                ;
        }

        getCity();
         $('.province').change(function(){
            getCity();
         });

        function getCity(){
            var id = $('.province').val();
            var arr_city = JSON.parse($('#arr_city').val());
            var text = '';
            for(var i=0; i<arr_city.length; i++){
                if(arr_city[i]['province_id'] == id){
                    text += '<option value="'+arr_city[i]['id']+'">'+arr_city[i]['name']+'</option>';
                }
            }
            // text += '';
            console.log(text);
            $('.city2 .select2-container--bootstrap .select2-selection--single .select2-selection__rendered').text('');
            $('.city').text('');
            $('.city').val('');
            // $('.city').prop('disabled', false);
            $('.city').append(text);
        }
    // });
  </script>
@endpush
@endsection
