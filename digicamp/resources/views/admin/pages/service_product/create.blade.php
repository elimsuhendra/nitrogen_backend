@extends($view_path.'.layouts.master')
@section('content')
@section('content')
<style>
 /* .single-image-card{
    max-width: 1024px !important;
    max-height: 714px !important;
  }*/
</style>
<!-- croppie -->
<link rel = "stylesheet" href="{{asset('components/back/css/croppie.css')}}" type="text/css">
<!-- <link rel = "stylesheet" href="{{asset('components/back/css/demo.css')}}" type="text/css"> -->
<!-- croppie -->
@push('styles')
<style>
  .modal-dialog {
    width: 1300px;
    height: 100%;
    margin: 0;
    padding: 0;
  }

  .modal-content {
    height: auto;
    min-height: 100%;
    border-radius: 0;
  }
  
  .canvas-image_card{
    width: 1100px;
    height: 800px;
  }
</style>

<form role="form" method="post" action="{{url($path)}}" enctype="multipart/form-data">
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'service_name','label' => 'Service Name','value' => (old('service_name') ? old('service_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6'])!!}

            <div class="col-md-6">
              <label for="tag">Service Category <span class="required no-margin-bottom" aria-required="true"></span></label>
              <div class="form-group form-md-line-input no-padding-top">
                <select class="select2" name="service_category" class="service_category" id="service_category">
                    @foreach($service_category as $sc)
                      <option value="{{$sc->category_service_name}}">{{$sc->category_service_name}}</option>
                    @endforeach
                </select>
              </div>
            </div>

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'price_name','label' => 'Service Price Name','value' => (old('price_name') ? old('price_name') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => ''])!!}

            {!!view($view_path.'.builder.text',['type' => 'text','name' => 'price','label' => 'Service Price (Rp.)','value' => (old('service_price') ? old('price') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'service_price'])!!}
      </div>
      </div>
      <hr/>
      <div class="row">
            <div class="col-md-12">
              <div class="col-md-6">
                <label for="tag">Product <span class="required no-margin-bottom" aria-required="true"></span></label>
                <div class="form-group form-md-line-input no-padding-top">
                  <select class="select2" name="product" class="product" id="product">
                      <option value="0">Select Product</option>
                      @foreach($product as $p)
                        <option value="{{$p->id}}">{{$p->product_name}}</option>
                      @endforeach
                  </select>
                </div>
              </div>

              <div class="col-md-6">

                <div class="form-group form-md-line-input" rrc="">

                  <input type="number" class="form-control qty_default" name="qty_default" id="qty_default" value="" placeholder="Qty Default">

                  <label for="form_floating_RRC">Qty Default <span class="required" aria-required="true"></span></label>

                  <small></small>

                </div>
              </div>
            </div>

            <div class="col-md-12">

              {!!view($view_path.'.builder.button',['type' => 'button','label' => 'Add','class' => 'add-product'])!!}

            </div>

            <hr/>

            <div class="table-responsive redeem-auto col-md-12">

              <table class="table table-bordered">

                <thead>
                  <th>Product Name</th>
                  <th>Qty Default</th>
                  <th>Action</th>

                </thead>

                <tbody class="product-data">

                </tbody>

            </table>

            </div>

            <div class="col-md-12 actions">
              {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}
          </div>
      </div>
</form>

@push('scripts')

@endpush
@push('custom_scripts')
  <script>
    $(document).ready(function(){
      //onclick event add remove outlet
      $(document).on('click','.add-product',function(){
        var product_name      = $('#product option:selected').text();
        var product_id        = $('#product').val();
        var qty_default       = $('#qty_default').val();
        console.log(product_name);
        console.log(qty_default);
        if(product_id == 0){
            alert('Please select product !');
        }else if(qty_default < 1 ){
            alert('Qty default must greater than 0 !');
        }else{
            var temp              = '<tr><td>'+product_name+'<input type="hidden" name="product_id[]" value="'+product_id+'"></td><td>'+qty_default+'<input type="hidden" name="qty_default[]" value="'+qty_default+'"></td><td><button type="button" class="btn btn-danger delete-product"><i class="fa fa-trash"></i></button></td></tr>';
            $('.product-data').append(temp);
        }
      })

      $(document).on('click','.delete-product',function(){
        $(this).closest('tr').remove();
      });

      $( ".service_price" ).blur(function() {  
          // alert('test');
          //number-format the user input
          var val   = $(this).val();
          var val2  = $.formatRupiah(val);
          $(this).val(val2);             
      });

    });
  </script>
@endpush
@endsection
