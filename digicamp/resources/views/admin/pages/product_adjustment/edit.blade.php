@extends($view_path.'.layouts.master')
@section('content')

@push('styles')
<style>

</style>

<form role="form" method="post" action="{{url($path)}}/{{$data->id}}" enctype="multipart/form-data">
{{ method_field('PUT') }}
  <div class="portlet light bordered">
    <div class="portlet-title">
      <div class="caption font-green">
        <i class="icon-layers font-green title-icon"></i>
        <span class="caption-subject bold uppercase"> {{$title}}</span>
      </div>
      <div class="actions">
        <a href="{{url($path)}}"><button type="button" class="btn red-mint">{{trans('general.back')}}</button></a>
      </div>
    </div>
    <div class="portlet-body form">
      @include('admin.includes.errors')
      <div class="row">        
        <div class="col-md-3">
              <ul class="list-unstyled profile-nav">
                  <li>
                      <img src="{{asset($image_path.'/'.$data->images)}}" onerror="this.src='{{asset($image_path2.'/'.'none.png') }}';" alt="" class="img-responsive">
                  </li>
              </ul>
          </div>
          <div class="col-md-9">
              <div class="row">
                   <div class="col-md-8 profile-info">
                      <h1 class="font-green sbold uppercase">{{ $data->product_name ? $data->product_name : '' }}</h1>
                      <p>
                          <i class="fa fa-money"></i> Rp. <span class="price">{{ $data->price ? $data->price : '-' }}</span>
                      </p>
                       <p>
                          <i class="">Unit</i> {{ $data->unit ? $data->unit : '' }}
                      </p>
                      <p>
                          <i class="fa fa-pencil"></i> {{ $data->description ? $data->description : '' }}
                      </p>
                  </div>
              </div>
              <!--end row-->
          </div>
      </div>

      <div class="row">
          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'buying_price','label' => 'Buying Price (Rp.)','value' => (old('buying_price') ? old('buying_price') : ''),'attribute' => 'required autofocus','form_class' => 'col-md-6', 'class' => 'buying_price'])!!}

          {!!view($view_path.'.builder.text',['type' => 'text','name' => 'buying_price_average','label' => 'Buying Price Average (Rp.)','value' => (old('buying_price_average') ? old('buying_price_average') : (count($buying_price_average) > 0 ? $buying_price_average->global_buying_price_average : 0)),'attribute' => 'required autofocus disabled','form_class' => 'col-md-6', 'class' => 'global_buying_price_average'])!!}

          <div class="col-md-12">
              <table id="outlet_adjust" class="table table-bordered table-form">
                <thead>
                  <tr class="parent">
                    <td colspan="3" class="cinema_col" align="center"><b>Outlet Stock Adjustment</b></td>
                  </tr>
                  <tr class="">
                    <td class="" align=""><b>Outlet Name</b></td>
                    <td class="" align=""><b>Qty</b></td>
                    <td class="" align=""><b>Adjust</b></td>
                  </tr>
                </thead>

                <tbody>
                  @foreach($outlet as $c)
                    <tr id="outlet_{{$c->id}}" valign="middle">
                      <td>{{$c->outlet_name}} <input type='hidden' name='outlet_id[]' value='{{$c->id}}'></td>
                      @php
                        $qty = 0;
                        foreach($adjust as $ad){
                          ($ad->outlet_id == $c->id ? $qty = $ad->outlet_stock_total : '');
                        }
                      @endphp
                        <td>{{$qty}} <input type='hidden' name='outlet_stock_total[]' value='{{$c->id}}'></td>
                      <td> <input class='form-control adjust' type='number' name='adjust[]' value=''></td>
                    </tr>
                  @endforeach
                </tbody>
              </table>  

               <div class="col-md-12 actions">
                    {!!view($view_path.'.builder.button',['type' => 'submit', 'class' => 'btn green','label' => trans('general.submit'),'ask' => 'y'])!!}   
                </div>
          </div>
      </div>
    </div>
  </div>
</form>
@endsection

@push('custom_scripts')
  <script>
    $(document).ready(function(){
      // $('input,select,textarea,checkbox,.remove-single-image').prop('disabled',true);
      tinymce.settings = $.extend(tinymce.settings, { readonly: 1 });

       var price = $('.price');
        if(price.text() != '-'){
            console.log(price.text());
            price.text($.formatRupiah(price.text()));
        }

        var global_buying_price_average = $('.global_buying_price_average');
        // console.log($.formatRupiah(global_buying_price_average.val()));
        global_buying_price_average.val($.formatRupiah(global_buying_price_average.val()));
    });

    $( ".buying_price" ).blur(function() {  
        // alert('test');
        //number-format the user input
        var val = $(this).val();
        var val2 = parseFloat(val.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $(this).val(val2);             
    });

    $( ".adjust" ).blur(function() {
        var val = $(this).val();
        console.log(val);
        if(val < 0){
          $(this).val(0);
        }
    });  
  </script>
@endpush
