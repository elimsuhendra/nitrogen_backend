<?php namespace digipos\Http\Controllers\Api;

use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use digipos\models\User;
use digipos\models\Category;
use digipos\models\Slideshow;
use digipos\models\Socialmedia;
use digipos\models\Payment_method;

class IndexController extends Controller {

	public function __construct(){
		parent::__construct();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function booting(){
		$this->res['global']			= $this->data;
		$this->res['social_media']		= $this->cache_query('social_media',Socialmedia::where('status','y')->orderBy('order','asc')->select('image','url'),'get');
		$this->res['payment_methods']	= $this->cache_query('payment_method',Payment_method::where('status','y')->orderBy('order','asc')->select('payment_method_name','image'),'get');

		try {
			$user 	= JWTAuth::parseToken()->authenticate();
	        if (!$user) {
	        	$this->res['user'] 	= ['status' => 'user_not_found'];
	        }else{
	        	$this->res['user'] 	= ['status' => 'success','user' => $user];
	        }
			return response()->json($this->res);
	    } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {
		    $this->res['user']		= ['status' => 'token_expired'];
			return response()->json($this->res);
		} catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
		    $this->res['user']		= ['status' => 'token_invalid'];
			return response()->json($this->res);
		} catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {
		    $this->res['user']		= ['status' => 'token_absent'];
			return response()->json($this->res);
		}
	}

	public function index(){
		$this->res['slideshow']	= Slideshow::where('status','y')->orderBy('order','asc')->get();
		return response()->json($this->res);
	}
}
