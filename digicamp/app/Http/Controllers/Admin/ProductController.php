<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Outlet;
use digipos\models\Adjustemnt;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class ProductController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Product";
		$this->data['title']	= $this->title;
		$this->root_link 		= "manage-product";
		$this->model 			= new Product;

		$this->bulk_action			= true;
		$this->bulk_action_data 	= [3];
		$this->image_path 			= 'components/both/images/product/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $desc_filter = Order_status::select('desc')->whereIn('id', [1,2,3,4,5,6,11])->get();

		// foreach($desc_filter as $dc){
		// 	$dc_filter[$dc->desc] = $dc->desc;
		// }

		$this->field = [
			[
				'name' => 'images',
				'label' => 'Image',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path, 'custom_path_id' => 'y']
			],
			[
				'name' 		=> 'product_name',
				'label' 	=> 'Product Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'unit',
				'label' 	=> 'Unit',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];

		// $this->model = $this->model->join('order_status', 'order_status.id', 'orderhd.order_status')->where('type_order', 'not like', '%post%')->select('orderhd.*', 'order_status.desc');
		return $this->build('index');
	}

	public function create(){
		
		$this->data['title'] 			= "Create product";
		$this->data['unit']  			= $this->unit;
		// $this->data['province']		= Province::get();

		return $this->render_view('pages.product.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'name' 		=> 'required|unique:product,product_name',
		]);
		$last_id 							= $this->model->orderBy('id', 'desc')->first();
		$curr_id 							= $last_id->id + 1;
		$this->model->product_name			= $request->name;
		$this->model->description			= $request->description;
		$this->model->unit					= $request->unit;
		$this->model->price					= $this->decode_rupiah($request->price);
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		($request->daily_report == 'y' ? $this->model->flag_daily_report = 'y' : $this->model->flag_daily_report = 'n');

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path.$curr_id.'/']
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();

		$this->increase_version();

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "Edit Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['data']  			= $this->model;
		// $this->data['province']		= Province::get();

		return $this->render_view('pages.product.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
			'name' 		=> 'required|unique:product,product_name,'.$id,
			'price' 	=> 'required',
		]);

		$this->model 						= $this->model->find($id);
		$this->model->product_name			= $request->name;
		$this->model->description			= $request->description;
		$this->model->unit					= $request->unit;
		$this->model->price					= $this->decode_rupiah($request->price);
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;


		if($request->input('remove-single-image-image') == 'y'){
			if($this->model->images != NULL){
				File::delete($this->image_path.$this->model->id.'/'.$this->model->images);
				$this->model->images = '';
			}
		}

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();
		$this->increase_version();
		
		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['data']  			= $this->model;
		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
