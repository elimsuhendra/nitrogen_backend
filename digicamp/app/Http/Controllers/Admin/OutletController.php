<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Authmenu;
use digipos\models\Useraccess;
use digipos\models\User;
use digipos\models\Config;
use digipos\models\Outlet;
use digipos\models\City;


// use Request;
use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;


class OutletController extends KyubiController{
	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard);
		$this->middleware($this->role_guard);
		$this->primary_field	= 'id';
		$this->title			= 'Outlet';
		$this->root_link		= 'manage-outlet';
		$this->model			= new Outlet;
		$this->user				= new User;
		
		// $this->delete_relation	= ['store'];
		$this->bulk_action		= true;
		$this->bulk_action_data = [2];
		$this->image_path 		= 'components/both/images/outlet/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 		= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	public function index(){
		$this->field = [
			[
				'name' => 'images',
				'label' => 'Image',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path],
			],
			[
				'name' 		=> 'outlet_name',
				'label' 	=> 'Outlet Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			],
		];

		$this->model = $this->model;
		return $this->build('index');
	}

	public function field_create(){
		$field = [
			[
				'name' => 'service_name',
				'label' => 'Service Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			]
		];
		return $field;
	}

	public function field_edit(){
		$field = [
			[
				'name' => 'service_name',
				'label' => 'Service Name',
				'type' => 'text',
				'attribute' => 'required',
				'validation' => 'required',
				'tab' => 'general',
			]
		];
		return $field;
	}

	public function create(){
		
		$this->data['title'] 			= "Create Outlet";
		$this->data['city']  			= City::get();
		// $this->data['province']		= Province::get();

		return $this->render_view('pages.outlet.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'name' 		=> 'required|unique:outlet,outlet_name',
		]);

		$this->model->outlet_name			= $request->name;
		$this->model->address				= $request->address;
		$this->model->city					= $request->city;
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->data['title'] = "View Outlet";
		$this->model 						= $this->model->find($id);

		$this->data['data']					= $this->model;
		return $this->render_view('pages.outlet.view');
	}

	public function edit($id){
		$this->data['title'] 				= "Edit Outlet";
		$this->model 						= $this->model->find($id);

		$this->data['data']					= $this->model;
		// dd($this->data['data']);
		$this->data['city']  				= City::get();

		return $this->render_view('pages.outlet.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
				'name' 		=> 'required|unique:outlet,outlet_name,'.$id,
				'city' 		=> 'required',
		]);

		$this->model						= $this->model->find($id);
		$this->model->outlet_name			= $request->name;
		$this->model->address				= $request->address;
		$this->model->city					= $request->city;


		if($request->input('remove-single-image-image') == 'y'){
			if($this->model->images != NULL){
				File::delete($this->image_path.$this->model->images);
				$this->model->images = '';
			}
		}

		if ($request->hasFile('image')){
        	// File::delete($this->image_path.$this->model->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path]
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;
		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully edit Outlet');
		return redirect()->to($this->data['path']);
	}							

	public function destroy(Request $request){
		// return $this->build('delete');

		$id = $request->id;
		$uc = $this->model->find($id);
		
		$uc->delete();
		Alert::success('Cinema has been deleted');
		return redirect()->back();
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function get_user_access(){
		$q = $this->build_array(Useraccess::where('id','>',1)->get(),'id','access_name');
		return $q;
	}

	public function export(){
		return $this->build_export();
	}

	public function sorting(){
		$this->field = [
			[
				'name' 		=> 'merchant_name',
				'label' 	=> 'Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text',
				'type' 		=> 'text'
			],
			[
				'name' 		=> 'status',
				'label' 	=> 'Status',
				'sorting' 	=> 'y',
				'search' => 'select',
				'search_data' => ['y' => 'Active', 'n' => 'Not-active'],
				'type' => 'check'
			]
		];
		$this->model = $this->model->where('id','>','1');
		return $this->build('sorting');
	}

	public function dosorting(){
		return $this->dosorting();
	}

	public function get_merchant_category($par){
		$data = $this->merchant_category
			->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
			->join('city','city.id','=','msmerchant.id_city')
			->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
			->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$par.' DAY)'))
			->where('merchant_log.created_at','<=',DB::raw('NOW()')); 
		$merchant_category 	= $data
							->select('merchant_category.*', 'msmerchant.merchant_name', 'msmerchant.id_merchant_category','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->groupBy('msmerchant.id_merchant_category')
							->get();
		return json_encode($merchant_category);
	}

	public function get_merchant_location($par){
		$merchant_location 	= $this->merchant_category
							->select('msmerchant.merchant_name', 'msmerchant.id_merchant_category', 'msmerchant.id_city','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', DB::raw('COUNT(*) as visits'))
							->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
							->join('city','city.id','=','msmerchant.id_city')
							->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
							->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL '.$par.' DAY)'))
							->where('merchant_log.created_at','<=',DB::raw('NOW()'))
							->orderBy('msmerchant.id_city')
							->groupBy('city.id', 'msmerchant.id_city')
							->get();
		return json_encode($merchant_location);
	}
}
?>