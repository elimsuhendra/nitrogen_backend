<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use Carbon\Carbon;

use digipos\models\Outlet;
use digipos\models\Product_adjustment;
use digipos\models\Product;
use digipos\models\Transactionhd;
use digipos\models\Transactiondt;
use digipos\models\User;
use digipos\models\User_absence;
use digipos\models\Shift;
use digipos\models\Service_product;
use digipos\models\Service_product_dt;


use digipos\Libraries\Alert;
use digipos\Libraries\Timeelapsed;
use Illuminate\Http\Request;

class OutletIncomeReportController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Laporan Stok Oulet";
		$this->root_url			= "report/stock-report";
		$this->root_link 		= "stock-report";
		$this->model 			= new Transactionhd;
		$this->product 			= new Product;
		$this->outlet 			= new Outlet;
		$this->data['root_url']	= $this->root_url;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->data['title'] 	= $this->title;
		$this->data['data1'] 	= $this->model->get();
		$this->data['outlet'] 	= Outlet::where('status', 'y')->get();
		$this->data['operator'] = User::where('status', 'y')->where('outlet_id', '!=', NULL)->get();
		// dd($this->data['operator']);
		return $this->render_view('pages.reports.outlet_income_report');
	}

	public function ext(request $request, $action){
		return $this->$action($request);
	}

	public function filter($request){
		$query = $this->model->join('transactiondt', 'transactiondt.transactionhd_id', 'transactionhd.id')->join('user', 'user.id', 'transactionhd.user_id')->leftJoin('user_absence', 'user_absence.id', 'transactionhd.user_absence_id')->leftJoin('shift', 'shift.id', 'user_absence.shift_id')->join('outlet', 'outlet.id', 'transactionhd.outlet_id');

		$qry = $this->model->join('transactiondt', 'transactiondt.transactionhd_id', 'transactionhd.id')->join('user', 'user.id', 'transactionhd.user_id')->leftJoin('user_absence', 'user_absence.id', 'transactionhd.user_absence_id')->leftJoin('shift', 'shift.id', 'user_absence.shift_id')->join('outlet', 'outlet.id', 'transactionhd.outlet_id');

		// $query_trhd = $this->model->join('user', 'user.id', 'transactionhd.user_id')->join('outlet', 'outlet.id', 'transactionhd.outlet_id');
		$service_product 	= Service_product::Join('service_product_dt', 'service_product_dt.service_product_id', 'service_product.id')->join('product', 'product.id', 'service_product_dt.product_id')->where('service_product.status', 'y')->select('service_product.*', 'service_product_dt.product_id as product_id', 'service_product_dt.service_product_id as service_product_id', 'product.product_name as product_name', 'product.price as product_price', 'product.unit as unit');
		
		$product 			= $this->product->where('status', 'y');
		foreach ($service_product->get() as $key => $sp) {
			$product->where('id', '!=', $sp->product_id);	
		}

		if($request->input('search_date_from') != ""){
			$search_from = $this->displayToSql($request->input('search_date_from'));
			$query->whereDate('transactionhd.created_at', '>=', $search_from);
			$qry->whereDate('transactionhd.created_at', '>=', $search_from);
			// $query_trhd->whereDate('transactionhd.created_at', '>=', $search_from);
		}

		if($request->input('search_date_to') != ""){
			$search_to = $this->displayToSql($request->input('search_date_to'));
			$query->whereDate('transactionhd.created_at', '<=', $search_to);
			$qry->whereDate('transactionhd.created_at', '<=', $search_to);
			// $query_trhd->whereDate('transactionhd.created_at', '<=', $search_to);
		}

		if($request->input('outlet_id') != ""){
			$outlet = Outlet::where('id', $request->input('outlet_id'))->first();
			$query->where('outlet.id', $request->input('outlet_id'))->get();
			$qry->where('outlet.id', $request->input('outlet_id'))->get();
			// $query_trhd->where('outlet.id', $request->input('outlet_id'))->get();
		}else{
			return '<div style="text-align:center;color:red;">Please select outlet !</div>';
		}

		if($request->input('operator_id') != ""){
			$operator = User::where('id', $request->input('operator_id'))->first();
			$query->where('user.id', $request->input('operator_id'))->get();
			$qry->where('user.id', $request->input('operator_id'))->get();
			// $query_trhd->where('user.id', $request->input('operator_id'))->get();
		}else{
			return '<div style="text-align:center;color:red;">Please select operator !</div>';
		}

		$display 		= "";
		$get_last 		= "";
		$count_product 	= 0;

		$query->select('transactionhd.*', 'outlet.outlet_name as outlet_name', 'user.name as op_name', 'transactiondt.product_name as ps_name', 'transactiondt.product_id as product_id','transactiondt.type as type', 'transactiondt.price as price', 'transactiondt.qty as qty', 'shift.shift_name as shift_name', DB::Raw('DATE(ni_transactiondt.created_at) created_date'))->orderBy('transactiondt.created_at');

		$arr = $query->select('transactionhd.*', 'outlet.outlet_name as outlet_name', 'user.name as op_name', 'transactiondt.product_name as ps_name', 'transactiondt.product_id as product_id','transactiondt.type as type', 'transactiondt.price as price', 'transactiondt.qty as qty', 'shift.shift_name as shift_name', DB::Raw('DATE(ni_transactiondt.created_at) created_date'))->orderBy('transactiondt.created_at')->get();
		// dd($arr);
		$qry->select('transactionhd.*', 'outlet.outlet_name as outlet_name', 'user.name as op_name', 'transactiondt.product_name as ps_name', 'transactiondt.product_id as product_id','transactiondt.type as type', 'transactiondt.price as price', 'transactiondt.qty as qty', 'shift.shift_name as shift_name');

		// $query_trhd2 = $query_trhd->select('transactionhd.*', 'outlet.outlet_name as outlet_name', 'user.name as op_name')->get();

		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		              	<tr>
		              		<th>LAPORAN PENDAPATAN '.$operator->name.' OUTLET - '.$outlet->outlet_name.'</th>
		              	</tr>
		              	<tr>
		              		<th>'.date_format(date_create($request->input('search_date_from')), 'd-M-Y').' s/d '.date_format(date_create($request->input('search_date_to')), 'd-M-Y').'</th>
		              	</tr>
		              </thead>
		              <tbody>'; 
		$display .= '<tr>
						<td class="bg-blue-madison font-white table-heading" style="text-align:center;font-weight: bold;" rowspan="2">Tanggal</td>
		                <td class="bg-blue-madison font-white table-heading" style="text-align:center;font-weight: bold;" rowspan="2">Shift</td>';

		// $query2 = $query->orderBy('transactiondt.created_at');	

		$arr_pro_ser = [];
		$service_product2 = $service_product->groupBy('service_product_id')->get();

		$display2       = '<tr>';
		
		foreach($service_product2 as $key => $sp){
			$display 		.= '<td class="bg-blue-madison font-white table-heading" style="text-align:center;font-weight: bold;" colspan="2">'.$sp->service_name.'</td>';
			$display2       .= '<td class="bg-blue-madison font-white table-heading table-heading" style="text-align:center;font-weight: bold;">'.$sp->product_name.' @'.$sp->product_price.'</td>';
			$display2       .= '<td class="bg-blue-madison font-white table-heading table-heading" style="text-align:center;font-weight: bold;">Jasa @'.$sp->price.'</td>';
		}

		$product2 		= $product->get(); 
		foreach($product2 as $key => $pr){
			$display .= '<td class="bg-blue-madison font-white table-heading" style="text-align:center;font-weight: bold;">'.$pr->product_name.'</td>';
			$display2 .= '<td class="bg-blue-madison font-white table-heading" style="text-align:center;font-weight: bold; rowspan="2">@'.$pr->price.'</td>';
		}

		$display2 .= '</tr>';
		$display .=  '<td class="bg-blue-madison font-white table-heading" style="text-align:center;font-weight: bold;" rowspan="2">Jumlah</td>
		                <td class="bg-blue-madison font-white table-heading" style="text-align:center;font-weight: bold;" rowspan="2">N2</td>';

		$display .= '</tr>'.$display2;

		$display4 		= '';
		$query2	= $query->groupBy('created_date');
		$qry2	= $qry;
		// dd($query2);																						
		if(count($query2->get()) > 0){
			foreach ($query2->get() as $key1 => $qr3) {
				$display5 		= '';
				$display4       .= '<tr>
									<td style="text-align:right;">'.date_format(date_create($qr3->created_at),'d-M-Y').'</td>
									<td style="text-align:right;">'.$qr3->shift_name.'</td>';
				$jumlah 		= 0;
				$jumlah_n2 		= 0;
				
				// generate type transactiondt jasa
				foreach($service_product2 as $key => $sp){
					$qty 			= 0;
					// $query2_jasa = $query2->where('type', 'Jasa')->where('product_id', $sp->id)->whereDate('transactiondt.created_at', '=',date_format(date_create($qr3->created_at),'Y-m-d'));
					// dd($query2_jasa);
					if(count($arr) > 0){
						foreach($arr as $key2 => $p3){
							if($p3->type == 'Jasa' && $p3->product_id == $sp->id && date_format(date_create($qr3->created_at),'Y-m-d') == date_format(date_create($p3->created_at),'Y-m-d')){

								$qty 	+= $p3->qty;
								$jumlah += ($p3->qty * $sp->price) + ($p3->qty * $sp->product_price);
								if($sp->unit == 'Psi'){
									$jumlah_n2 += ($p3->qty * $sp->price) + ($p3->qty * $sp->product_price);
								}
							}
						}
						$display5 	.= 	'<td id="jasa-'.$sp->id.'" style="text-align:right;">'.$qty.'</td>
										<td id="jasa-'.$sp->id.'" style="text-align:right;">'.$qty.'</td>';
					}else{
						$display5 	.= 	'<td id="jasa-'.$sp->id.'" style="text-align:right;">0</td>
											<td id="jasa-'.$sp->id.'" style="text-align:right;">0</td>';
					}
				}

				// generate type transactiondt produk
				// dd($product2);	
				foreach($product2 as $key => $pr){
					$qty 				= 0;
					$jumlah2 			= 0;
					// dd($qry2->where('type', 'Produk')->where('product_id', '18')->whereDate('transactiondt.created_at', date_format(date_create($qr3->created_at),'Y-m-d'))->get());
					// $query2_produk = $qry2->where('type', 'Produk')->where('product_id', $pr->id)->whereDate('transactiondt.created_at', date_format(date_create($qr3->created_at),'Y-m-d'));
					if($pr->id == '18'){
						// dd($arr);
					}
					if(count($arr) > 0){
						foreach($arr as $key2 => $p3){
							if($p3->type == 'Produk' && $p3->product_id == $pr->id && date_format(date_create($qr3->created_at),'Y-m-d') == date_format(date_create($p3->created_at),'Y-m-d')){
								$qty 	+= $p3->qty;
								$jumlah += $p3->qty * $pr->price;
								if($pr->unit == 'Psi'){
									$jumlah_n2 += $p3->qty * $pr->price;
								}
							}
						}

						$display5 	.= '<td id="produk-'.$pr->id.'" style="text-align:right;">'.$qty.'</td>';
					}else{
						$display5 	.= '<td id="produk-'.$pr->id.'" style="text-align:right;">0</td>';
					}
				}
				
				$display5 	.= '<td id="produk-'.$p3->id.'" style="text-align:right;">'.$jumlah.'</td>
								<td id="produk-'.$p3->id.'" style="text-align:right;">'.$jumlah_n2.'</td>';
				$display4 .= $display5.'</tr>';
			}
		}
		$display .= $display4;

		$display .='</tbody>
					</table>
					</div>';

		return $display;
	}
}
