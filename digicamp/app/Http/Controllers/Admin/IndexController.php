<?php namespace digipos\Http\Controllers\Admin;
use digipos\Libraries\Email;

use digipos\models\Province;
use digipos\models\City;

use DB;

class IndexController extends Controller {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard); 
		$this->data['title'] 	= "Dashboard";
		// $this->merchant 		= new Msmerchant;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $this->data['top_order_success']				=	Mitra::select('mitra.*', DB::raw('COUNT(*) as mitra_count'))->join('orderdt', 'mitra.id', 'orderdt.mitra_id')->where('orderdt.status_order', '6')->groupBy('mitra.name')->orderBy('mitra_count', 'desc')->limit(5)->get();
		// // dd($this->data['top_order_success']);
		// $this->data['top_mitra_rating']					=	Mitra::select('mitra.*', 'mitradt.total_rating', 'mitradt.total_user_rating')->join('mitradt','mitra.id','mitradt.mitra_id')->orderBy('mitradt.total_rating', 'desc')->orderBy('mitradt.total_user_rating','desc')->limit(5)->get();
		// // dd($this->data['top_mitra_rating']);

		// $this->data['top_customer_order_success']		= Customer::select('customer.*', 'orderhd.customer_id', DB::raw('COUNT(*) as count'))->join('orderhd','orderhd.customer_id','customer.id')->where('order_status', '6')->groupBy('customer.id')->get();
		// // dd($this->data['top_customer_order_success']);

		return $this->render_view('pages.index');
	}

}
