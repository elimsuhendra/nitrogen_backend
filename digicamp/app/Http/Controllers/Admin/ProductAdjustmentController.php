<?php namespace digipos\Http\Controllers\Admin;

use digipos\models\Config;
use digipos\models\Product;
use digipos\models\Outlet;
use digipos\models\Product_adjustment;

use Validator;
use Auth;
use Hash;
use DB;
use digipos\Libraries\Alert;
use Illuminate\Http\Request;
use digipos\Libraries\Email;
use Carbon\Carbon;
use File;

class ProductAdjustmentController extends KyubiController {

	public function __construct()
	{
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Adjustment";
		$this->data['title']	= $this->title;
		$this->root_link 		= "adjustment";
		$this->model 			= new Product;
		$this->outlet 			= Outlet::where('status', 'y')->get();

		$this->bulk_action			= false;
		// $this->bulk_action_data 	= [3];
		$this->image_path 			= 'components/both/images/product/';
		$this->data['image_path'] 	= $this->image_path;
		$this->image_path2 			= 'components/both/images/web/';
		$this->data['image_path2'] 	= $this->image_path2;
		$this->unit 				= ['Pcs','Psi'];

		$this->meta_title = Config::where('name', 'web_title')->first();
        $this->meta_description = Config::where('name', 'web_description')->first();
        $this->meta_keyword = Config::where('name', 'web_keywords')->first();
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(){
		// $desc_filter = Order_status::select('desc')->whereIn('id', [1,2,3,4,5,6,11])->get();

		// foreach($desc_filter as $dc){
		// 	$dc_filter[$dc->desc] = $dc->desc;
		// }

		$this->field = [
			[
				'name' => 'images',
				'label' => 'Image',
				'type' => 'image',
				'file_opt' => ['path' => $this->image_path, 'custom_path_id' => 'y']
			],
			[
				'name' 		=> 'product_name',
				'label' 	=> 'Product Name',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			[
				'name' 		=> 'unit',
				'label' 	=> 'Unit',
				'sorting' 	=> 'y',
				'search' 	=> 'text'
			],
			// [
			// 	'name'			=> 'updated_at',
			// 	'label'			=> 'Adjustment Datetime',
			// 	'belongto'		=> ['method' => 'product_adjustment', 'field' => 'updated_at'],
			// 	'sorting' 		=> 'y',
			// 	// 'search'		=> 'select',
			// 	// 'search_data' 	=> $st_order,
			// ]
		];

		return $this->build('index');
	}

	public function create(){
		
		$this->data['title'] 			= "Create product";
		$this->data['unit']  			= $this->unit;
		// $this->data['province']		= Province::get();

		return $this->render_view('pages.product.create');
	}

	public function store(Request $request){
		$this->validate($request,[
			'name' 		=> 'required|unique:product,product_name',
		]);

		$this->model->product_name			= $request->name;
		$this->model->description			= $request->description;
		$this->model->unit					= $request->unit;
		$this->model->price					= $this->decode_rupiah($request->price);
		$this->model->status 				= 'y';
		$this->model->upd_by 				= auth()->guard($this->guard)->user()->id;

		if ($request->hasFile('image')){
        	// File::delete($path.$user->images);
			$data = [
						'name' => 'image',
						'file_opt' => ['path' => $this->image_path.$this->model->id.'/']
					];
			$image = $this->build_image($data);
			$this->model->images = $image;
		}

		// dd($this->model);
		$this->model->save();

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function edit($id){
		$this->model 						= $this->model->find($id);
		$this->data['title'] 				= "Edit Product ".$this->model->product_name;
		$this->data['unit']  				= $this->unit;
		$this->data['outlet']  				= Outlet::where('status', 'y')->get();
		$this->data['adjust']  				= Product_adjustment::where('id', DB::raw("(select max(`id`) from ni_product_adjustment npa where outlet_id = ni_product_adjustment.outlet_id and product_id = $id)"))->orderBy('id', 'desc')->groupBy('outlet_id')->get();
		// dd($this->data['adjust']);
		$this->data['buying_price_average'] = Product_adjustment::where('product_id', $id)->orderBy('product_adjustment.id', 'desc')->first();
		// dd($this->data['buying_price_average']);
		// dd($this->data['adjust']);
		$this->data['data']  			= $this->model;
		// $this->data['province']		= Province::get();

		return $this->render_view('pages.product_adjustment.edit');
	}

	public function update(Request $request, $id){
		$this->validate($request,[
			'buying_price' 		=> 'required',
		]);
		
		if($request->buying_price == 'NaN'){
			Alert::fail('Buying Price must numeric !');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		}

		
		$this->model 					= $this->model->find($id);
		$buying_price					= $this->decode_rupiah($request->buying_price);

		$upd_by 						= auth()->guard($this->guard)->user()->id;

		$product_adjustment  			= Product_adjustment::where('id', DB::raw("(select max(`id`) from ni_product_adjustment npa where outlet_id = ni_product_adjustment.outlet_id)"))->where('product_adjustment.product_id', $id)->orderBy('id', 'desc')->groupBy('outlet_id')->get();
		// dd($product_adjustment);
		$adjust 						= $request->adjust;
		// dd($adjust);
		$data = [];

		$temp_arr = [];
		$global_stock_total_prev 				= 0;
		$global_stock_total_prev2 				= 0;
		$global_buying_price_average_prev 		= 0;
		$global_buying_price_average_prev2 		= 0;
		$flagStatus 							= 0;

		$outlet_stock_total 			= 0;
		$qty 							= 0;
		$global_stock_total				= 0;
		$outlet_buying_price_average 	= 0;
		$global_buying_price_average 	= 0;
		$buying_price2					= 0;
		$buying_price_prev				= 0;
		$flagFilledOutlet 				= 0;
		foreach ($this->outlet as $key => $dy) {
			if($adjust[$key] != ''){
				$flagFilledOutlet = 1;
				var_dump('$adjust[$key]: '.$adjust[$key]);
				if(count($product_adjustment) > 0){
					$flagExist = 0;
					foreach($product_adjustment as $key2 => $p){
						//get last product adjustment
						if($key2 == 0){
							if($global_stock_total_prev == 0){
								$global_stock_total_prev 			= $p->global_stock_total;
							}

							if($global_buying_price_average_prev == 0){
								$global_buying_price_average_prev	=  $p->global_buying_price_average;
							}

							if($global_stock_total_prev2 == 0){
								$global_stock_total_prev2			=  $p->global_stock_total;
							} 

							if($buying_price_prev == 0){
								$buying_price_prev 					= $p->outlet_buying_price;
							}
						}

						if($p->outlet_id == $dy->id && $p->product_id == $this->model->id){
							$flagExist = 1;

							$qty 							= $adjust[$key] - $p->outlet_stock_total;
							$outlet_stock_total				= $qty + $p->outlet_stock_total;
							if($global_stock_total_prev != 0){
								$global_stock_total				= $qty + $global_stock_total_prev;
							}else{
								$global_stock_total				= $qty + $global_stock_total;
							}
							

							if($qty > 0){
								var_dump('global_stock_total_prev: '.$global_stock_total_prev);
								var_dump('global_buying_price_average_prev: '.$global_buying_price_average_prev);
								if($global_stock_total != 0){
									$buying_price2 					= $buying_price;
									$global_buying_price_average	= (($global_stock_total_prev * 
									$global_buying_price_average_prev) + ($qty * $buying_price2))/ ($global_stock_total);
								}
							}else{
								$buying_price2 					= $global_buying_price_average_prev;
								if($global_stock_total != 0){
									$global_buying_price_average	= (($global_buying_price_average_prev * $global_stock_total_prev) + ($qty * $global_buying_price_average_prev)) /  $global_stock_total;
								}
							}
						}
					}

					//if this outlet and product didn't find in previous database
					if($flagExist == 0){
						$outlet_stock_total 			= $adjust[$key];
						$qty 							= $adjust[$key];
						$global_stock_total				= $adjust[$key] + $global_stock_total_prev;
						$buying_price2 					= $buying_price;
						var_dump('$global_stock_total_prev: '.$global_stock_total_prev);
						var_dump('$global_buying_price_average_prev: '.$global_buying_price_average_prev);
						$global_buying_price_average	= (($global_stock_total_prev * $global_buying_price_average_prev) + ($qty * $buying_price))/ ($global_stock_total);
					}
				}else{
					$outlet_stock_total 			= $adjust[$key];
					$qty 							= $adjust[$key];
					$global_stock_total				= $adjust[$key] + $global_stock_total_prev;
					$global_buying_price_average 	= $buying_price;
					$buying_price2 					= $buying_price;
				}
				var_dump('$qty: '.$qty);
				if($qty != 0){
					$temp_arr[]			= [
						'outlet_id'						=> $dy->id,
						'product_id'					=> $this->model->id,
						'qty'							=> $qty,
						'outlet_stock_total'			=> $outlet_stock_total,
						'global_stock_total'			=> $global_stock_total,
						'outlet_buying_price'			=> $buying_price2,
						'global_buying_price_average'	=> $global_buying_price_average,
						'status'						=> 'adjust',
						'upd_by'						=> $upd_by,
						'created_at'					=> Carbon::now(),
						'updated_at'					=> Carbon::now()

					];

					$stock = 0;
					$global_stock_total_prev 	 		= $global_stock_total;
					$global_buying_price_average_prev2 	= $global_buying_price_average;
					$global_buying_price_average_prev 	= $global_buying_price_average;
					$global_stock_total_prev2 			= $global_stock_total;
					$buying_price_prev 					= $buying_price2;
					var_dump('$global_stock_total_prev2_2: '.$global_stock_total_prev2.'<br>');
				}
			}
		}
		if($flagFilledOutlet == 0){
			Alert::fail('No outlet filled !');
			return redirect()->to($this->data['path'].'/'.$id.'/edit')->withInput($request->input());
		} 
		// dd($temp_arr);
		if(count($temp_arr) > 0){
			Product_adjustment::insert($temp_arr);
		}

		// dd($this->model);
		// $this->model->save();

		Alert::success('Successfully add new Outlet');
		return redirect()->to($this->data['path']);
	}

	public function show($id){
		$this->model 					= $this->model->find($id);
		$this->data['title'] 			= "View Product ".$this->model->product_name;
		$this->data['unit']  			= $this->unit;
		$this->data['data']  			= $this->model;
		return $this->render_view('pages.product.view');
	}

	public function ext($action){
		return $this->$action();
	}

	public function updateflag(){
		// dd('bulkupda');
		return $this->buildupdateflag();
	}

	public function bulkupdate(){
		return $this->buildbulkedit();
	}

	public function export(){
		return $this->build_export_cus();
	}
}
