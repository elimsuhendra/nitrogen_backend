<?php namespace digipos\Http\Controllers\Admin;

use DB;
use Session;
use Hash;
use Carbon\Carbon;

use digipos\models\Order_hd;
use digipos\models\Order_dt;
use digipos\models\Mitra;
use digipos\models\User;
use digipos\models\Useraccess;

use digipos\Libraries\Alert;
use digipos\Libraries\Timeelapsed;
use Illuminate\Http\Request;

class ReportMitraController extends KyubiController {

	public function __construct(){
		parent::__construct();
		$this->middleware($this->auth_guard); 
		$this->middleware($this->role_guard);
		$this->title 			= "Laporan Order";
		$this->root_url			= "report/orders-mitra";
		$this->root_link 		= "orders-mitra";
		$this->model 			= new Order_hd;
		$this->model2			= new Mitra;
		$this->data['root_url']	= $this->root_url;
	}

	/**source.
	 *
	 * @return Response
	 * Display a listing of the response
	 */
	public function index(){
		$this->data['title'] = $this->title;
		$this->data['data1'] = $this->model->get();
		$this->data['mitra'] = $this->model2->get();
		return $this->render_view('pages.reports.orders_mitra');
	}

	public function ext(request $request, $action){
		return $this->$action($request);
	}

	public function filter($request){
		$query = Order_dt::join('orderhd', 'orderhd.id', 'orderdt.orderhd_id')->join('order_address', 'order_address.orderhd_id', 'orderhd.id')->join('mitra', 'orderdt.mitra_id', 'mitra.id')->orderBy('orderhd.id', 'desc');

		if($request->input('search_date_from') != ""){
			$search_from = $this->displayToSql($request->input('search_date_from'));
			$query->whereDate('orderhd.order_date', '>=', $search_from);
		}

		if($request->input('search_date_to') != ""){
			$search_to = $this->displayToSql($request->input('search_date_to'));
			$query->whereDate('orderhd.order_date', '<=', $search_to);
		}

		if($request->input('mitra_id') != ""){
			$query->whereIn('orderdt.mitra_id', $request->input('mitra_id'));
		}

		if($request->input('format') != ""){
			$query->where('orderhd.type_order', 'like', '%'.$request->input('format').'%');
		}

		$display = "";
		$get_last = "";

		$order = $query->select('orderhd.transaction_id', 'orderhd.name as cst_name', 'orderhd.order_date', 'orderdt.service_name', 'orderdt.qty', 'orderdt.price', 'orderdt.total_price as oddt_total_pr', 'mitra.name as mt_name')->get();

		$display .='<div class="table-scrollable">
		            <table id="table-laporan" class="table table-hover table-light">
		              <thead>
		                <tr>
		                  <th class="bg-blue-madison font-white">Customer Name</th>
		                  <th class="bg-blue-madison font-white">Service Name</th>
		                  <th class="bg-blue-madison font-white">Qty</th>
		                  <th class="bg-blue-madison font-white">Price</th>
		                  <th class="bg-blue-madison font-white">Total Price</th>
		                </tr>
		              </thead>
		              <tbody>';

		foreach($order as $odr){
			if($get_last == ""){
				$display .= '<tr>
								<td colspan="6" class="bg-dark font-white">'.$odr->transaction_id.' - '.$odr->mt_name.'<br>'.date('d - F - Y', strtotime($odr->order_date)).'</td>
							</tr>';
				$get_last = $odr->transaction_id;

				foreach($order as $odc){
					if($get_last == $odc->transaction_id) {
						$display .= '<tr>
										<td>'.$odc->cst_name.'</td>
										<td>'.$odc->service_name.'</td>
										<td>'.$odc->qty.'</td>
										<td>Rp. '. number_format($odc->price).'</td>
										<td>Rp. '. number_format($odc->oddt_total_pr).'</td>
									</tr>';
					}
				}

				$display .= '<tr><td colspan="6">&nbsp;</td></tr>';
			} else{
				if($get_last != $odr->transaction_id){
					$display .= '<tr>
								<td colspan="6" class="bg-dark font-white">'.$odr->transaction_id.' - '.$odr->mt_name.'<br>'.date('d - F - Y', strtotime($odr->order_date)).'</td>
							</tr>';
					$get_last = $odr->transaction_id;

					foreach($order as $odc){
						if($get_last == $odc->transaction_id) {
							$display .= '<tr>
											<td>'.$odc->cst_name.'</td>
											<td>'.$odc->service_name.'</td>
											<td>'.$odc->qty.'</td>
											<td>Rp. '. number_format($odc->price).'</td>
											<td>Rp. '. number_format($odc->oddt_total_pr).'</td>
										</tr>';
										
						}
					}

					$display .= '<tr><td colspan="6">&nbsp;</td></tr>';
				}
			}
		}

		$display .='</tbody>
					</table>
					</div>';

		return $display;
	}
}
