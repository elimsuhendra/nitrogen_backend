<?php namespace digipos\Http\Controllers\Front;

use digipos\models\Slideshow;
use digipos\models\Msmerchant;
use digipos\models\Merchant_category;
use digipos\models\Merchant_log;
use digipos\models\City;
use digipos\models\Content;
use digipos\models\Featured_product_hd;
use digipos\models\Home;
use digipos\models\News;
use DB;

class IndexController extends ShukakuController {

	public function __construct(){
		parent::__construct();
		$this->data['header_info']	= 'Home';
		$this->menu = $this->data['path'][0];
		$this->data['menu'] 		= $this->menu;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */

	public function index(){
		dd('Under maintain');
		$this->data['slide_show'] 	= Slideshow::get();
		$this->data['alasan_1'] 		= Home::where('name', 'alasan_1')->first();
		$this->data['alasan_1_name'] 	= Home::where('name', 'alasan_1_name')->first();
		$this->data['alasan_2'] 		= Home::where('name', 'alasan_2')->first();
		$this->data['alasan_2_name'] 	= Home::where('name', 'alasan_2_name')->first();
		$this->data['alasan_3'] 		= Home::where('name', 'alasan_3')->first();
		$this->data['alasan_3_name'] 	= Home::where('name', 'alasan_3_name')->first();
		$merchant_category				= Merchant_category::
											select('msmerchant.merchant_name', 'msmerchant.id_merchant_category', 'msmerchant.id_city','city.name as city_name','merchant_log.ip_address', 'merchant_log.created_at', 'merchant_category.category_name','merchant_category.image', DB::raw('COUNT(*) as visits'))
											->join('msmerchant','msmerchant.id_merchant_category','=','merchant_category.id')
											->join('city','city.id','=','msmerchant.id_city')
											->join('merchant_log','merchant_log.merchant_id','=','msmerchant.id')
											// ->where('merchant_log.created_at','>=',DB::raw('DATE_SUB(CURDATE(), INTERVAL 7 DAY)'))
											// ->where('merchant_log.created_at','<=',DB::raw('NOW()'))
											->where('merchant_category.status', 'y')
											->orderBy('visits', 'desc')
											->groupBy('merchant_category.id');
		$this->data['headline_merchant_category']	= Merchant_category::where('headline','y')->orderBy('updated_at', 'desc')->first();
		// dd($this->data['headline_merchant']);
		$this->data['headline_merchant_category2']	= $merchant_category->where('id_merchant_category', '!=',$this->data['headline_merchant_category']->id)->limit(6)->get();
		// dd($this->data['sticky_merchant']);
		$this->data['sticky_merchant']		= 	Msmerchant::select('msmerchant.*', 'city.name as city_name')
											->join('city', 'city.id', '=', 'msmerchant.id_city')
											->where('status','y')
											->orderBy('created_at', 'desc')
											->limit(3)
											->get();

		$this->data['headline_news']	= 	News::
											where('status','y')
											->where('headline_news', 'y')
											->orderBy('updated_at', 'desc')
											->first();

		$this->data['news']				= 	News::
											where('status','y')
											->where('id', '!=', $this->data['headline_news']->id)
											->orderBy('created_at', 'desc')
											->limit(6)
											->get();

		$this->data['sticky_news']		= 	News::
											where('status','y')
											->where('sticky_news', 'y')
											->orderBy('updated_at', 'desc')
											->limit(3)
											->get();
		return $this->render_view('pages.index');
	}
}
