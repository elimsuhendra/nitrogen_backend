<?php namespace digipos\models;

use Illuminate\Database\Eloquent\Model;

class Product_adjustment extends Model{
	protected $table = 'product_adjustment';
	protected $product = 'digipos\models\Product';

	public function product(){
        return $this->belongsTo($this->product,'product_id');
    }
}