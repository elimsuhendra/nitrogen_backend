/*
Navicat MySQL Data Transfer

Source Server         : jukir
Source Server Version : 50719
Source Host           : 139.59.237.28:3306
Source Database       : nitrogen

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2017-10-19 15:52:53
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ni_authmenu`
-- ----------------------------
DROP TABLE IF EXISTS `ni_authmenu`;
CREATE TABLE `ni_authmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `user_access_id` int(11) DEFAULT NULL,
  `create` varchar(1) DEFAULT NULL,
  `edit` varchar(1) DEFAULT NULL,
  `view` varchar(1) DEFAULT NULL,
  `delete` varchar(1) DEFAULT NULL,
  `sorting` varchar(1) DEFAULT NULL,
  `export` varchar(1) DEFAULT NULL,
  `upd_user` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2021 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_authmenu
-- ----------------------------
INSERT INTO `ni_authmenu` VALUES ('2020', '5', '1', 'n', 'n', 'y', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2019', '2', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2018', '132', '1', 'n', 'n', 'y', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2017', '131', '1', 'n', 'n', 'y', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2016', '8', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2015', '4', '1', 'y', 'y', 'y', 'y', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2014', '3', '1', 'y', 'y', 'y', 'y', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2013', '12', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2012', '129', '1', 'y', 'y', 'y', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2011', '130', '1', 'y', 'y', 'y', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2010', '111', '1', 'n', 'y', 'y', 'n', 'y', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2009', '7', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2008', '112', '1', 'y', 'y', 'y', 'n', 'y', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2007', '6', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');
INSERT INTO `ni_authmenu` VALUES ('2006', '1', '1', 'n', 'n', 'n', 'n', 'n', 'n', '1', '2017-09-28 16:54:55', '2017-09-28 16:54:55');

-- ----------------------------
-- Table structure for `ni_category_service`
-- ----------------------------
DROP TABLE IF EXISTS `ni_category_service`;
CREATE TABLE `ni_category_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_service_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_category_service
-- ----------------------------
INSERT INTO `ni_category_service` VALUES ('1', 'Mobil', null, '2017-10-09 13:42:49');
INSERT INTO `ni_category_service` VALUES ('17', 'Motor', '0000-00-00 00:00:00', '2017-10-09 13:43:10');
INSERT INTO `ni_category_service` VALUES ('18', 'Jeep', '0000-00-00 00:00:00', '2017-10-09 13:43:07');

-- ----------------------------
-- Table structure for `ni_city`
-- ----------------------------
DROP TABLE IF EXISTS `ni_city`;
CREATE TABLE `ni_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(10) DEFAULT NULL,
  `city_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=502 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_city
-- ----------------------------
INSERT INTO `ni_city` VALUES ('1', '21', '1', 'Aceh Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('2', '21', '2', 'Aceh Barat Daya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('3', '21', '3', 'Aceh Besar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('4', '21', '4', 'Aceh Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('5', '21', '5', 'Aceh Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('6', '21', '6', 'Aceh Singkil', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('7', '21', '7', 'Aceh Tamiang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('8', '21', '8', 'Aceh Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('9', '21', '9', 'Aceh Tenggara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('10', '21', '10', 'Aceh Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('11', '21', '11', 'Aceh Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('12', '32', '12', 'Agam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('13', '23', '13', 'Alor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('14', '19', '14', 'Ambon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('15', '34', '15', 'Asahan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('16', '24', '16', 'Asmat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('17', '1', '17', 'Badung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('18', '13', '18', 'Balangan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('19', '15', '19', 'Balikpapan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('20', '21', '20', 'Banda Aceh', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('21', '18', '21', 'Bandar Lampung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('22', '9', '22', 'Bandung Kabupaten', '2016-06-02 11:12:54', '2017-09-27 14:50:53');
INSERT INTO `ni_city` VALUES ('23', '9', '23', 'Bandung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('24', '9', '24', 'Bandung Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('25', '29', '25', 'Banggai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('26', '29', '26', 'Banggai Kepulauan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('27', '2', '27', 'Bangka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('28', '2', '28', 'Bangka Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('29', '2', '29', 'Bangka Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('30', '2', '30', 'Bangka Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('31', '11', '31', 'Bangkalan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('32', '1', '32', 'Bangli', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('33', '13', '33', 'Banjar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('34', '9', '34', 'Banjar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('35', '13', '35', 'Banjarbaru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('36', '13', '36', 'Banjarmasin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('37', '10', '37', 'Banjarnegara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('38', '28', '38', 'Bantaeng', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('39', '5', '39', 'Bantul', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('40', '33', '40', 'Banyuasin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('41', '10', '41', 'Banyumas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('42', '11', '42', 'Banyuwangi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('43', '13', '43', 'Barito Kuala', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('44', '14', '44', 'Barito Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('45', '14', '45', 'Barito Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('46', '14', '46', 'Barito Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('47', '28', '47', 'Barru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('48', '17', '48', 'Batam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('49', '10', '49', 'Batang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('50', '8', '50', 'Batang Hari', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('51', '11', '51', 'Batu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('52', '34', '52', 'Batu Bara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('53', '30', '53', 'Bau-Bau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('54', '9', '54', 'Bekasi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('55', '9', '55', 'Bekasi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('56', '2', '56', 'Belitung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('57', '2', '57', 'Belitung Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('58', '23', '58', 'Belu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('59', '21', '59', 'Bener Meriah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('60', '26', '60', 'Bengkalis', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('61', '12', '61', 'Bengkayang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('62', '4', '62', 'Bengkulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('63', '4', '63', 'Bengkulu Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('64', '4', '64', 'Bengkulu Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('65', '4', '65', 'Bengkulu Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('66', '15', '66', 'Berau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('67', '24', '67', 'Biak Numfor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('68', '22', '68', 'Bima', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('69', '22', '69', 'Bima', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('70', '34', '70', 'Binjai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('71', '17', '71', 'Bintan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('72', '21', '72', 'Bireuen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('73', '31', '73', 'Bitung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('74', '11', '74', 'Blitar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('75', '11', '75', 'Blitar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('76', '10', '76', 'Blora', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('77', '7', '77', 'Boalemo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('78', '9', '78', 'Bogor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('79', '9', '79', 'Bogor', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('80', '11', '80', 'Bojonegoro', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('81', '31', '81', 'Bolaang Mongondow (Bolmong)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('82', '31', '82', 'Bolaang Mongondow Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('83', '31', '83', 'Bolaang Mongondow Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('84', '31', '84', 'Bolaang Mongondow Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('85', '30', '85', 'Bombana', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('86', '11', '86', 'Bondowoso', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('87', '28', '87', 'Bone', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('88', '7', '88', 'Bone Bolango', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('89', '15', '89', 'Bontang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('90', '24', '90', 'Boven Digoel', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('91', '10', '91', 'Boyolali', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('92', '10', '92', 'Brebes', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('93', '32', '93', 'Bukittinggi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('94', '1', '94', 'Buleleng', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('95', '28', '95', 'Bulukumba', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('96', '16', '96', 'Bulungan (Bulongan)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('97', '8', '97', 'Bungo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('98', '29', '98', 'Buol', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('99', '19', '99', 'Buru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('100', '19', '100', 'Buru Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('101', '30', '101', 'Buton', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('102', '30', '102', 'Buton Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('103', '9', '103', 'Ciamis', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('104', '9', '104', 'Cianjur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('105', '10', '105', 'Cilacap', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('106', '3', '106', 'Cilegon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('107', '9', '107', 'Cimahi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('108', '9', '108', 'Cirebon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('109', '9', '109', 'Cirebon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('110', '34', '110', 'Dairi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('111', '24', '111', 'Deiyai (Deliyai)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('112', '34', '112', 'Deli Serdang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('113', '10', '113', 'Demak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('114', '1', '114', 'Denpasar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('115', '9', '115', 'Depok', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('116', '32', '116', 'Dharmasraya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('117', '24', '117', 'Dogiyai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('118', '22', '118', 'Dompu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('119', '29', '119', 'Donggala', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('120', '26', '120', 'Dumai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('121', '33', '121', 'Empat Lawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('122', '23', '122', 'Ende', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('123', '28', '123', 'Enrekang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('124', '25', '124', 'Fakfak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('125', '23', '125', 'Flores Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('126', '9', '126', 'Garut', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('127', '21', '127', 'Gayo Lues', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('128', '1', '128', 'Gianyar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('129', '7', '129', 'Gorontalo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('130', '7', '130', 'Gorontalo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('131', '7', '131', 'Gorontalo Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('132', '28', '132', 'Gowa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('133', '11', '133', 'Gresik', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('134', '10', '134', 'Grobogan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('135', '5', '135', 'Gunung Kidul', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('136', '14', '136', 'Gunung Mas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('137', '34', '137', 'Gunungsitoli', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('138', '20', '138', 'Halmahera Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('139', '20', '139', 'Halmahera Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('140', '20', '140', 'Halmahera Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('141', '20', '141', 'Halmahera Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('142', '20', '142', 'Halmahera Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('143', '13', '143', 'Hulu Sungai Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('144', '13', '144', 'Hulu Sungai Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('145', '13', '145', 'Hulu Sungai Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('146', '34', '146', 'Humbang Hasundutan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('147', '26', '147', 'Indragiri Hilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('148', '26', '148', 'Indragiri Hulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('149', '9', '149', 'Indramayu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('150', '24', '150', 'Intan Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('151', '6', '151', 'Jakarta Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('152', '6', '152', 'Jakarta Pusat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('153', '6', '153', 'Jakarta Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('154', '6', '154', 'Jakarta Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('155', '6', '155', 'Jakarta Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('156', '8', '156', 'Jambi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('157', '24', '157', 'Jayapura', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('158', '24', '158', 'Jayapura', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('159', '24', '159', 'Jayawijaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('160', '11', '160', 'Jember', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('161', '1', '161', 'Jembrana', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('162', '28', '162', 'Jeneponto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('163', '10', '163', 'Jepara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('164', '11', '164', 'Jombang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('165', '25', '165', 'Kaimana', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('166', '26', '166', 'Kampar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('167', '14', '167', 'Kapuas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('168', '12', '168', 'Kapuas Hulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('169', '10', '169', 'Karanganyar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('170', '1', '170', 'Karangasem', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('171', '9', '171', 'Karawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('172', '17', '172', 'Karimun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('173', '34', '173', 'Karo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('174', '14', '174', 'Katingan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('175', '4', '175', 'Kaur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('176', '12', '176', 'Kayong Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('177', '10', '177', 'Kebumen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('178', '11', '178', 'Kediri', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('179', '11', '179', 'Kediri', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('180', '24', '180', 'Keerom', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('181', '10', '181', 'Kendal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('182', '30', '182', 'Kendari', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('183', '4', '183', 'Kepahiang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('184', '17', '184', 'Kepulauan Anambas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('185', '19', '185', 'Kepulauan Aru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('186', '32', '186', 'Kepulauan Mentawai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('187', '26', '187', 'Kepulauan Meranti', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('188', '31', '188', 'Kepulauan Sangihe', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('189', '6', '189', 'Kepulauan Seribu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('190', '31', '190', 'Kepulauan Siau Tagulandang Biaro (Sitaro)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('191', '20', '191', 'Kepulauan Sula', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('192', '31', '192', 'Kepulauan Talaud', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('193', '24', '193', 'Kepulauan Yapen (Yapen Waropen)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('194', '8', '194', 'Kerinci', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('195', '12', '195', 'Ketapang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('196', '10', '196', 'Klaten', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('197', '1', '197', 'Klungkung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('198', '30', '198', 'Kolaka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('199', '30', '199', 'Kolaka Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('200', '30', '200', 'Konawe', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('201', '30', '201', 'Konawe Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('202', '30', '202', 'Konawe Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('203', '13', '203', 'Kotabaru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('204', '31', '204', 'Kotamobagu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('205', '14', '205', 'Kotawaringin Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('206', '14', '206', 'Kotawaringin Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('207', '26', '207', 'Kuantan Singingi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('208', '12', '208', 'Kubu Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('209', '10', '209', 'Kudus', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('210', '5', '210', 'Kulon Progo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('211', '9', '211', 'Kuningan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('212', '23', '212', 'Kupang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('213', '23', '213', 'Kupang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('214', '15', '214', 'Kutai Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('215', '15', '215', 'Kutai Kartanegara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('216', '15', '216', 'Kutai Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('217', '34', '217', 'Labuhan Batu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('218', '34', '218', 'Labuhan Batu Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('219', '34', '219', 'Labuhan Batu Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('220', '33', '220', 'Lahat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('221', '14', '221', 'Lamandau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('222', '11', '222', 'Lamongan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('223', '18', '223', 'Lampung Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('224', '18', '224', 'Lampung Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('225', '18', '225', 'Lampung Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('226', '18', '226', 'Lampung Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('227', '18', '227', 'Lampung Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('228', '12', '228', 'Landak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('229', '34', '229', 'Langkat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('230', '21', '230', 'Langsa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('231', '24', '231', 'Lanny Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('232', '3', '232', 'Lebak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('233', '4', '233', 'Lebong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('234', '23', '234', 'Lembata', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('235', '21', '235', 'Lhokseumawe', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('236', '32', '236', 'Lima Puluh Koto/Kota', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('237', '17', '237', 'Lingga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('238', '22', '238', 'Lombok Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('239', '22', '239', 'Lombok Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('240', '22', '240', 'Lombok Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('241', '22', '241', 'Lombok Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('242', '33', '242', 'Lubuk Linggau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('243', '11', '243', 'Lumajang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('244', '28', '244', 'Luwu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('245', '28', '245', 'Luwu Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('246', '28', '246', 'Luwu Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('247', '11', '247', 'Madiun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('248', '11', '248', 'Madiun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('249', '10', '249', 'Magelang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('250', '10', '250', 'Magelang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('251', '11', '251', 'Magetan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('252', '9', '252', 'Majalengka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('253', '27', '253', 'Majene', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('254', '28', '254', 'Makassar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('255', '11', '255', 'Malang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('256', '11', '256', 'Malang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('257', '16', '257', 'Malinau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('258', '19', '258', 'Maluku Barat Daya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('259', '19', '259', 'Maluku Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('260', '19', '260', 'Maluku Tenggara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('261', '19', '261', 'Maluku Tenggara Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('262', '27', '262', 'Mamasa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('263', '24', '263', 'Mamberamo Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('264', '24', '264', 'Mamberamo Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('265', '27', '265', 'Mamuju', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('266', '27', '266', 'Mamuju Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('267', '31', '267', 'Manado', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('268', '34', '268', 'Mandailing Natal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('269', '23', '269', 'Manggarai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('270', '23', '270', 'Manggarai Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('271', '23', '271', 'Manggarai Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('272', '25', '272', 'Manokwari', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('273', '25', '273', 'Manokwari Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('274', '24', '274', 'Mappi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('275', '28', '275', 'Maros', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('276', '22', '276', 'Mataram', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('277', '25', '277', 'Maybrat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('278', '34', '278', 'Medan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('279', '12', '279', 'Melawi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('280', '8', '280', 'Merangin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('281', '24', '281', 'Merauke', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('282', '18', '282', 'Mesuji', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('283', '18', '283', 'Metro', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('284', '24', '284', 'Mimika', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('285', '31', '285', 'Minahasa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('286', '31', '286', 'Minahasa Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('287', '31', '287', 'Minahasa Tenggara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('288', '31', '288', 'Minahasa Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('289', '11', '289', 'Mojokerto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('290', '11', '290', 'Mojokerto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('291', '29', '291', 'Morowali', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('292', '33', '292', 'Muara Enim', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('293', '8', '293', 'Muaro Jambi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('294', '4', '294', 'Muko Muko', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('295', '30', '295', 'Muna', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('296', '14', '296', 'Murung Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('297', '33', '297', 'Musi Banyuasin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('298', '33', '298', 'Musi Rawas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('299', '24', '299', 'Nabire', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('300', '21', '300', 'Nagan Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('301', '23', '301', 'Nagekeo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('302', '17', '302', 'Natuna', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('303', '24', '303', 'Nduga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('304', '23', '304', 'Ngada', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('305', '11', '305', 'Nganjuk', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('306', '11', '306', 'Ngawi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('307', '34', '307', 'Nias', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('308', '34', '308', 'Nias Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('309', '34', '309', 'Nias Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('310', '34', '310', 'Nias Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('311', '16', '311', 'Nunukan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('312', '33', '312', 'Ogan Ilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('313', '33', '313', 'Ogan Komering Ilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('314', '33', '314', 'Ogan Komering Ulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('315', '33', '315', 'Ogan Komering Ulu Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('316', '33', '316', 'Ogan Komering Ulu Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('317', '11', '317', 'Pacitan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('318', '32', '318', 'Padang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('319', '34', '319', 'Padang Lawas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('320', '34', '320', 'Padang Lawas Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('321', '32', '321', 'Padang Panjang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('322', '32', '322', 'Padang Pariaman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('323', '34', '323', 'Padang Sidempuan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('324', '33', '324', 'Pagar Alam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('325', '34', '325', 'Pakpak Bharat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('326', '14', '326', 'Palangka Raya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('327', '33', '327', 'Palembang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('328', '28', '328', 'Palopo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('329', '29', '329', 'Palu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('330', '11', '330', 'Pamekasan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('331', '3', '331', 'Pandeglang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('332', '9', '332', 'Pangandaran', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('333', '28', '333', 'Pangkajene Kepulauan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('334', '2', '334', 'Pangkal Pinang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('335', '24', '335', 'Paniai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('336', '28', '336', 'Parepare', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('337', '32', '337', 'Pariaman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('338', '29', '338', 'Parigi Moutong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('339', '32', '339', 'Pasaman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('340', '32', '340', 'Pasaman Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('341', '15', '341', 'Paser', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('342', '11', '342', 'Pasuruan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('343', '11', '343', 'Pasuruan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('344', '10', '344', 'Pati', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('345', '32', '345', 'Payakumbuh', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('346', '25', '346', 'Pegunungan Arfak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('347', '24', '347', 'Pegunungan Bintang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('348', '10', '348', 'Pekalongan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('349', '10', '349', 'Pekalongan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('350', '26', '350', 'Pekanbaru', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('351', '26', '351', 'Pelalawan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('352', '10', '352', 'Pemalang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('353', '34', '353', 'Pematang Siantar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('354', '15', '354', 'Penajam Paser Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('355', '18', '355', 'Pesawaran', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('356', '18', '356', 'Pesisir Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('357', '32', '357', 'Pesisir Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('358', '21', '358', 'Pidie', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('359', '21', '359', 'Pidie Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('360', '28', '360', 'Pinrang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('361', '7', '361', 'Pohuwato', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('362', '27', '362', 'Polewali Mandar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('363', '11', '363', 'Ponorogo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('364', '12', '364', 'Pontianak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('365', '12', '365', 'Pontianak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('366', '29', '366', 'Poso', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('367', '33', '367', 'Prabumulih', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('368', '18', '368', 'Pringsewu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('369', '11', '369', 'Probolinggo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('370', '11', '370', 'Probolinggo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('371', '14', '371', 'Pulang Pisau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('372', '20', '372', 'Pulau Morotai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('373', '24', '373', 'Puncak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('374', '24', '374', 'Puncak Jaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('375', '10', '375', 'Purbalingga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('376', '9', '376', 'Purwakarta', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('377', '10', '377', 'Purworejo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('378', '25', '378', 'Raja Ampat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('379', '4', '379', 'Rejang Lebong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('380', '10', '380', 'Rembang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('381', '26', '381', 'Rokan Hilir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('382', '26', '382', 'Rokan Hulu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('383', '23', '383', 'Rote Ndao', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('384', '21', '384', 'Sabang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('385', '23', '385', 'Sabu Raijua', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('386', '10', '386', 'Salatiga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('387', '15', '387', 'Samarinda', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('388', '12', '388', 'Sambas', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('389', '34', '389', 'Samosir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('390', '11', '390', 'Sampang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('391', '12', '391', 'Sanggau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('392', '24', '392', 'Sarmi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('393', '8', '393', 'Sarolangun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('394', '32', '394', 'Sawah Lunto', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('395', '12', '395', 'Sekadau', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('396', '28', '396', 'Selayar (Kepulauan Selayar)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('397', '4', '397', 'Seluma', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('398', '10', '398', 'Semarang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('399', '10', '399', 'Semarang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('400', '19', '400', 'Seram Bagian Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('401', '19', '401', 'Seram Bagian Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('402', '3', '402', 'Serang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('403', '3', '403', 'Serang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('404', '34', '404', 'Serdang Bedagai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('405', '14', '405', 'Seruyan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('406', '26', '406', 'Siak', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('407', '34', '407', 'Sibolga', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('408', '28', '408', 'Sidenreng Rappang/Rapang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('409', '11', '409', 'Sidoarjo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('410', '29', '410', 'Sigi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('411', '32', '411', 'Sijunjung (Sawah Lunto Sijunjung)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('412', '23', '412', 'Sikka', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('413', '34', '413', 'Simalungun', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('414', '21', '414', 'Simeulue', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('415', '12', '415', 'Singkawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('416', '28', '416', 'Sinjai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('417', '12', '417', 'Sintang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('418', '11', '418', 'Situbondo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('419', '5', '419', 'Sleman', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('420', '32', '420', 'Solok', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('421', '32', '421', 'Solok', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('422', '32', '422', 'Solok Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('423', '28', '423', 'Soppeng', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('424', '25', '424', 'Sorong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('425', '25', '425', 'Sorong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('426', '25', '426', 'Sorong Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('427', '10', '427', 'Sragen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('428', '9', '428', 'Subang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('429', '21', '429', 'Subulussalam', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('430', '9', '430', 'Sukabumi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('431', '9', '431', 'Sukabumi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('432', '14', '432', 'Sukamara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('433', '10', '433', 'Sukoharjo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('434', '23', '434', 'Sumba Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('435', '23', '435', 'Sumba Barat Daya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('436', '23', '436', 'Sumba Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('437', '23', '437', 'Sumba Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('438', '22', '438', 'Sumbawa', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('439', '22', '439', 'Sumbawa Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('440', '9', '440', 'Sumedang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('441', '11', '441', 'Sumenep', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('442', '8', '442', 'Sungaipenuh', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('443', '24', '443', 'Supiori', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('444', '11', '444', 'Surabaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('445', '10', '445', 'Surakarta (Solo)', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('446', '13', '446', 'Tabalong', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('447', '1', '447', 'Tabanan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('448', '28', '448', 'Takalar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('449', '25', '449', 'Tambrauw', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('450', '16', '450', 'Tana Tidung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('451', '28', '451', 'Tana Toraja', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('452', '13', '452', 'Tanah Bumbu', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('453', '32', '453', 'Tanah Datar', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('454', '13', '454', 'Tanah Laut', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('455', '3', '455', 'Tangerang Kabupaten', '2016-06-02 11:12:54', '2017-09-27 14:46:50');
INSERT INTO `ni_city` VALUES ('456', '3', '456', 'Tangerang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('457', '3', '457', 'Tangerang Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('458', '18', '458', 'Tanggamus', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('459', '34', '459', 'Tanjung Balai', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('460', '8', '460', 'Tanjung Jabung Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('461', '8', '461', 'Tanjung Jabung Timur', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('462', '17', '462', 'Tanjung Pinang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('463', '34', '463', 'Tapanuli Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('464', '34', '464', 'Tapanuli Tengah', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('465', '34', '465', 'Tapanuli Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('466', '13', '466', 'Tapin', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('467', '16', '467', 'Tarakan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('468', '9', '468', 'Tasikmalaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('469', '9', '469', 'Tasikmalaya', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('470', '34', '470', 'Tebing Tinggi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('471', '8', '471', 'Tebo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('472', '10', '472', 'Tegal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('473', '10', '473', 'Tegal', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('474', '25', '474', 'Teluk Bintuni', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('475', '25', '475', 'Teluk Wondama', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('476', '10', '476', 'Temanggung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('477', '20', '477', 'Ternate', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('478', '20', '478', 'Tidore Kepulauan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('479', '23', '479', 'Timor Tengah Selatan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('480', '23', '480', 'Timor Tengah Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('481', '34', '481', 'Toba Samosir', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('482', '29', '482', 'Tojo Una-Una', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('483', '29', '483', 'Toli-Toli', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('484', '24', '484', 'Tolikara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('485', '31', '485', 'Tomohon', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('486', '28', '486', 'Toraja Utara', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('487', '11', '487', 'Trenggalek', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('488', '19', '488', 'Tual', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('489', '11', '489', 'Tuban', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('490', '18', '490', 'Tulang Bawang', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('491', '18', '491', 'Tulang Bawang Barat', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('492', '11', '492', 'Tulungagung', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('493', '28', '493', 'Wajo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('494', '30', '494', 'Wakatobi', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('495', '24', '495', 'Waropen', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('496', '18', '496', 'Way Kanan', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('497', '10', '497', 'Wonogiri', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('498', '10', '498', 'Wonosobo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('499', '24', '499', 'Yahukimo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('500', '24', '500', 'Yalimo', '2016-06-02 11:12:54', '0000-00-00 00:00:00');
INSERT INTO `ni_city` VALUES ('501', '5', '501', 'Yogyakarta', '2016-06-02 11:12:54', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `ni_config`
-- ----------------------------
DROP TABLE IF EXISTS `ni_config`;
CREATE TABLE `ni_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `value` text,
  `config_type` varchar(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_config
-- ----------------------------
INSERT INTO `ni_config` VALUES ('1', 'web_name', 'Beauty Belle', 'both', null, '2017-09-19 09:53:03');
INSERT INTO `ni_config` VALUES ('2', 'web_logo', 'TRB0qK_^B1763B57E8097F6119D04AA4A4FFD1F7E71CF62F722B8CD086^pimgpsh_fullsize_distr.png', 'both', null, '2017-09-19 09:53:04');
INSERT INTO `ni_config` VALUES ('3', 'web_email', 'testdevlop203@gmail.com', 'both', null, '2017-07-12 14:46:35');
INSERT INTO `ni_config` VALUES ('4', 'favicon', 'xhBNQY_^B1763B57E8097F6119D04AA4A4FFD1F7E71CF62F722B8CD086^pimgpsh_fullsize_distr.png', 'both', null, '2017-09-19 09:53:04');
INSERT INTO `ni_config` VALUES ('5', 'web_title', 'Frantinco Website', 'both', '0000-00-00 00:00:00', '2017-06-14 08:50:15');
INSERT INTO `ni_config` VALUES ('6', 'web_description', 'Frantinco Website', 'both', '0000-00-00 00:00:00', '2017-06-14 08:50:15');
INSERT INTO `ni_config` VALUES ('7', 'web_keywords', 'Frantinco Website', 'both', '0000-00-00 00:00:00', '2017-06-14 08:50:15');
INSERT INTO `ni_config` VALUES ('8', 'maintenance_mode', 'n', 'front', '0000-00-00 00:00:00', '2016-05-19 05:26:19');
INSERT INTO `ni_config` VALUES ('9', 'background_color', '#d622cf', 'front', '0000-00-00 00:00:00', '2016-05-29 09:25:41');
INSERT INTO `ni_config` VALUES ('10', 'font_color', '#2f9fd6', 'front', '0000-00-00 00:00:00', '2016-05-29 09:25:43');
INSERT INTO `ni_config` VALUES ('11', 'send_email', 'n', 'both', '0000-00-00 00:00:00', '2016-11-29 10:18:35');
INSERT INTO `ni_config` VALUES ('12', 'send_sms', 'n', 'both', '0000-00-00 00:00:00', '2016-11-29 09:07:11');
INSERT INTO `ni_config` VALUES ('13', 'deadline_payment', '1', 'both', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `ni_config` VALUES ('14', 'phone', '021-22553223', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `ni_config` VALUES ('15', 'address', '<p>Perkantoran Sedayu Square Blok C No 32<br />Cengkareng, Jakarta Barat</p>', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `ni_config` VALUES ('16', 'header_info', 'Happy Shopping Sista', 'front', '0000-00-00 00:00:00', '2016-12-03 03:59:47');
INSERT INTO `ni_config` VALUES ('17', 'shipping_returns_product', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>\r\n<ul>\r\n<li>Any Product types that You want - Simple, Configurable</li>\r\n<li>Downloadable/Digital Products, Virtual Products</li>\r\n<li>Inventory Management with Backordered items</li>\r\n<li>Customer Personal Products - upload text for embroidery, monogramming</li>\r\n<li>Create Store-specific attributes on the fly</li>\r\n</ul>', 'front', '0000-00-00 00:00:00', '2016-12-03 15:23:01');
INSERT INTO `ni_config` VALUES ('18', 'terms_condition_product', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit.</p>\r\n<ul>\r\n<li>Any Product types that You want - Simple, Configurable</li>\r\n<li>Downloadable/Digital Products, Virtual Products</li>\r\n<li>Inventory Management with Backordered items</li>\r\n<li>Customer Personal Products - upload text for embroidery, monogramming</li>\r\n<li>Create Store-specific attributes on the fly</li>\r\n</ul>', 'front', '0000-00-00 00:00:00', '2016-12-03 15:23:01');
INSERT INTO `ni_config` VALUES ('19', 'gmaps', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3967.0150615360963!2d106.72692481476865!3d-6.128674995562458!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e6a1d5201130ab3%3A0xe0586c3d9f378cd0!2sSolveway+Digital!5e0!3m2!1sid!2sid!4v1497410446355', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `ni_config` VALUES ('20', 'city', '151', 'front', '0000-00-00 00:00:00', '2017-05-31 09:59:28');
INSERT INTO `ni_config` VALUES ('21', 'fax', '', 'front', '0000-00-00 00:00:00', '2017-06-14 10:22:28');
INSERT INTO `ni_config` VALUES ('22', 'product_version', '4', 'both', '0000-00-00 00:00:00', '2017-10-17 17:24:48');
INSERT INTO `ni_config` VALUES ('23', 'about', 'Nitrogen', 'both', '0000-00-00 00:00:00', '2017-10-17 09:04:15');

-- ----------------------------
-- Table structure for `ni_daily_report`
-- ----------------------------
DROP TABLE IF EXISTS `ni_daily_report`;
CREATE TABLE `ni_daily_report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `absence_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `stock_karet_cacing` int(11) DEFAULT NULL,
  `stock_tabung` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approve` varchar(2) DEFAULT 'n',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_daily_report
-- ----------------------------

-- ----------------------------
-- Table structure for `ni_employee`
-- ----------------------------
DROP TABLE IF EXISTS `ni_employee`;
CREATE TABLE `ni_employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `address` text,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `shift` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_employee
-- ----------------------------

-- ----------------------------
-- Table structure for `ni_form_report`
-- ----------------------------
DROP TABLE IF EXISTS `ni_form_report`;
CREATE TABLE `ni_form_report` (
  `id` int(11) NOT NULL,
  `product_code` varchar(20) DEFAULT NULL,
  `field_name` varchar(50) DEFAULT NULL,
  `placeholder` varchar(50) DEFAULT NULL,
  `display_form` varchar(2) DEFAULT NULL,
  `images` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_form_report
-- ----------------------------
INSERT INTO `ni_form_report` VALUES ('1', 'TBA01', 'Sisa Stock Tabung dalam PSI', 'Tulis stock', 'y', null);
INSERT INTO `ni_form_report` VALUES ('2', 'KC001', 'Sisa Karet Cacing', 'Tulis Satuan Pcs', 'y', null);

-- ----------------------------
-- Table structure for `ni_mslanguage`
-- ----------------------------
DROP TABLE IF EXISTS `ni_mslanguage`;
CREATE TABLE `ni_mslanguage` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `language_name` varchar(30) DEFAULT NULL,
  `language_name_alias` varchar(30) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `order` int(10) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_mslanguage
-- ----------------------------
INSERT INTO `ni_mslanguage` VALUES ('1', 'Indonesia', 'id', 'y', '1', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `ni_mslanguage` VALUES ('2', 'English', 'en', 'y', '2', null, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `ni_msmenu`
-- ----------------------------
DROP TABLE IF EXISTS `ni_msmenu`;
CREATE TABLE `ni_msmenu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `heading` varchar(30) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `menu_name_alias` varchar(50) DEFAULT NULL,
  `menu_id` int(10) DEFAULT NULL,
  `status_parent` varchar(1) DEFAULT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `create` varchar(1) DEFAULT NULL,
  `edit` varchar(1) DEFAULT NULL,
  `view` varchar(1) DEFAULT NULL,
  `delete` varchar(1) DEFAULT NULL,
  `sorting` varchar(1) DEFAULT NULL,
  `export` varchar(1) DEFAULT NULL,
  `order` int(2) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=133 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_msmenu
-- ----------------------------
INSERT INTO `ni_msmenu` VALUES ('1', 'Control Menu', 'Dashboard', '/', null, 'y', 'fa fa-home', 'n', 'n', 'n', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-08-16 17:26:36');
INSERT INTO `ni_msmenu` VALUES ('2', null, 'Preferences', 'preferences', null, 'y', 'fa fa-wrench', 'n', 'n', 'n', 'n', 'n', 'n', '25', 'y', '0000-00-00 00:00:00', '2016-11-25 07:28:06');
INSERT INTO `ni_msmenu` VALUES ('3', null, 'User', 'user', '12', 'n', null, 'y', 'y', 'y', 'y', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2016-06-01 08:06:05');
INSERT INTO `ni_msmenu` VALUES ('4', null, 'User Access', 'user-access', '12', 'n', null, 'y', 'y', 'y', 'y', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2016-06-01 08:06:08');
INSERT INTO `ni_msmenu` VALUES ('5', null, 'General Settings', 'general-settings', '2', 'n', null, 'n', 'n', 'y', 'n', 'n', 'n', '4', 'y', '0000-00-00 00:00:00', '2016-11-29 10:19:50');
INSERT INTO `ni_msmenu` VALUES ('111', null, 'Adjustment', 'adjustment', '7', 'n', null, 'n', 'y', 'y', 'n', 'y', 'n', '3', 'y', '0000-00-00 00:00:00', '2017-09-28 16:55:10');
INSERT INTO `ni_msmenu` VALUES ('130', null, 'Service', 'service', '7', 'n', null, 'y', 'y', 'y', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-09-26 17:49:54');
INSERT INTO `ni_msmenu` VALUES ('129', null, 'Manage', 'manage-product', '7', 'n', '', 'y', 'y', 'y', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-09-26 18:04:52');
INSERT INTO `ni_msmenu` VALUES ('12', null, 'Administration', 'administration', null, 'y', 'fa fa-cog', 'n', 'n', 'n', 'n', 'n', 'n', '12', 'y', '0000-00-00 00:00:00', '2016-06-01 09:36:11');
INSERT INTO `ni_msmenu` VALUES ('6', null, 'Outlet', 'outlets', null, 'y', 'fa fa-flag', 'n', 'n', 'n', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-09-26 17:47:28');
INSERT INTO `ni_msmenu` VALUES ('131', null, 'Stock Report', 'stock-report', '8', 'n', null, 'n', 'n', 'y', 'n', 'n', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-10-19 10:44:34');
INSERT INTO `ni_msmenu` VALUES ('112', '', 'Manage', 'manage-outlet', '6', 'n', '', 'y', 'y', 'y', 'n', 'y', 'n', '1', 'y', '0000-00-00 00:00:00', '2017-09-26 18:04:23');
INSERT INTO `ni_msmenu` VALUES ('132', null, 'Outlet Income Report', 'outlet-income-report', '8', 'n', null, 'n', 'n', 'y', 'n', 'n', 'n', '2', 'y', '0000-00-00 00:00:00', '2017-10-19 10:55:11');
INSERT INTO `ni_msmenu` VALUES ('8', null, 'Report', 'report', null, 'y', 'fa fa-bar-chart', 'n', 'n', 'n', 'n', 'n', 'n', '12', 'y', '0000-00-00 00:00:00', '2017-09-26 18:03:40');
INSERT INTO `ni_msmenu` VALUES ('7', null, 'Product', 'products', null, 'y', 'fa fa-product', 'n', 'n', 'y', 'n', 'n', 'n', '9', 'y', '0000-00-00 00:00:00', '2017-09-26 17:49:52');

-- ----------------------------
-- Table structure for `ni_outlet`
-- ----------------------------
DROP TABLE IF EXISTS `ni_outlet`;
CREATE TABLE `ni_outlet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_name` varchar(100) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `address` text NOT NULL,
  `city` varchar(100) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_outlet
-- ----------------------------
INSERT INTO `ni_outlet` VALUES ('4', 'Keppo', 'http://2.bp.blogspot.com/-eAspzuKE3lk/VS0E3ir__pI/AAAAAAAAAKs/qZbRsoRi1PI/s1600/makeup15.jpg', '000000000000', 'y', 'Jalan Oliander', 'Jakarta Barat', null, '0000-00-00 00:00:00', '2017-09-13 21:51:56');
INSERT INTO `ni_outlet` VALUES ('5', 'Mitra', 'http://2.bp.blogspot.com/-eAspzuKE3lk/VS0E3ir__pI/AAAAAAAAAKs/qZbRsoRi1PI/s1600/makeup15.jpg', '000000000000', 'y', 'Jalan Oliander', 'Jakarta Barat', null, '0000-00-00 00:00:00', '2017-09-14 17:13:27');
INSERT INTO `ni_outlet` VALUES ('6', 'Mitra1', 'http://2.bp.blogspot.com/-eAspzuKE3lk/VS0E3ir__pI/AAAAAAAAAKs/qZbRsoRi1PI/s1600/makeup15.jpg', '000000000000', 'y', 'Jalan Oliander', 'Jakarta Barat', null, '0000-00-00 00:00:00', '2017-09-02 20:58:25');
INSERT INTO `ni_outlet` VALUES ('11', 'Mitra11', 'http://2.bp.blogspot.com/-eAspzuKE3lk/VS0E3ir__pI/AAAAAAAAAKs/qZbRsoRi1PI/s1600/makeup15.jpg', '000000000000', 'y', 'Jalan Oliander', 'Jakarta Barat', null, '0000-00-00 00:00:00', '2017-09-02 20:58:26');
INSERT INTO `ni_outlet` VALUES ('14', 'mitra2', 'UdXZD7_Hydrangeas.jpg', '08999878046', 'y', 'Jl. xyz', 'Jakarta Barat', '1', '2017-09-14 18:02:37', '2017-09-19 10:28:12');
INSERT INTO `ni_outlet` VALUES ('15', 'Kemanggisan', 'SpGJr9_Chrysanthemum.jpg', null, 'y', 'Jl. Kemanggisan Raya 9', '151', '1', '2017-09-27 14:14:48', '2017-09-27 14:14:48');
INSERT INTO `ni_outlet` VALUES ('16', 'Balaraja 2', '28U4l2_Koala.jpg', null, 'y', 'Jl. Balaraja 2', 'Tangerang Kabupaten', '1', '2017-09-27 14:20:56', '2017-09-27 15:41:16');

-- ----------------------------
-- Table structure for `ni_product`
-- ----------------------------
DROP TABLE IF EXISTS `ni_product`;
CREATE TABLE `ni_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL COMMENT 'pcs, psi',
  `description` text,
  `status` varchar(2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` varchar(100) DEFAULT NULL,
  `price` float(11,0) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `product_code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_product
-- ----------------------------
INSERT INTO `ni_product` VALUES ('16', 'karet cacing 2', 'Psi', 'abc', 'y', '2017-09-28 06:35:20', '2017-10-16 10:45:08', '1', '10000', 'lzZWJT_Hydrangeas.jpg', 'KC001');
INSERT INTO `ni_product` VALUES ('17', 'Karet Cacing', 'Pcs', 'Karet Cacing', 'y', '2017-10-10 17:05:28', '2017-10-17 17:13:50', null, '10000', null, 'KC002');
INSERT INTO `ni_product` VALUES ('18', 'Tabung Nitrogen', 'Psi', 'Tabung Nitrogen', 'y', '2017-10-10 17:06:51', '2017-10-10 17:07:04', null, '10000', null, 'TBA01');
INSERT INTO `ni_product` VALUES ('20', 'Karet Cacing 3', 'Pcs', 'Karet Cacing', 'y', '2017-10-10 17:05:28', '2017-10-17 17:13:53', null, '10000', null, 'KC003');

-- ----------------------------
-- Table structure for `ni_product_adjustment`
-- ----------------------------
DROP TABLE IF EXISTS `ni_product_adjustment`;
CREATE TABLE `ni_product_adjustment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_id` int(11) DEFAULT NULL,
  `product_id` int(50) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `outlet_stock_total` int(11) DEFAULT NULL,
  `global_stock_total` int(11) DEFAULT NULL,
  `outlet_buying_price` float(11,0) DEFAULT NULL,
  `outlet_buying_price_average` float(11,0) DEFAULT '0',
  `global_buying_price_average` float(11,0) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_product_adjustment
-- ----------------------------
INSERT INTO `ni_product_adjustment` VALUES ('2', '4', '16', '10', '10', '10', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 09:33:54', '1');
INSERT INTO `ni_product_adjustment` VALUES ('3', '5', '16', '10', '10', '20', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 09:33:54', '1');
INSERT INTO `ni_product_adjustment` VALUES ('4', '6', '16', '10', '10', '30', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 09:33:54', '1');
INSERT INTO `ni_product_adjustment` VALUES ('5', '11', '16', '10', '10', '40', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 09:33:54', '1');
INSERT INTO `ni_product_adjustment` VALUES ('6', '14', '16', '10', '10', '50', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 09:33:54', '1');
INSERT INTO `ni_product_adjustment` VALUES ('7', '15', '16', '10', '10', '60', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 09:33:54', '1');
INSERT INTO `ni_product_adjustment` VALUES ('8', '16', '16', '10', '10', '70', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 09:33:54', '1');
INSERT INTO `ni_product_adjustment` VALUES ('9', '4', '17', '10', '10', '10', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 17:20:35', '1');
INSERT INTO `ni_product_adjustment` VALUES ('10', '5', '17', '10', '10', '20', '10000', '0', '10000', 'adjust', '2017-10-10 09:33:54', '2017-10-10 17:20:37', '1');
INSERT INTO `ni_product_adjustment` VALUES ('11', '4', '18', '2000', '2000', '2000', '30000', '0', '30000', 'adjust', '2017-10-16 10:14:09', '2017-10-16 10:14:21', null);
INSERT INTO `ni_product_adjustment` VALUES ('12', '5', '18', '2000', '2000', '4000', '30000', '0', '30000', 'adjust', '2017-10-16 10:16:27', '2017-10-16 10:16:31', null);
INSERT INTO `ni_product_adjustment` VALUES ('13', '5', '17', '-1', '9', '19', '10000', '0', '10000', 'sale', '2017-10-17 17:27:47', '2017-10-17 17:27:47', null);
INSERT INTO `ni_product_adjustment` VALUES ('14', '5', '18', '-1', '1999', '3999', '30000', '0', '30000', 'sale', '2017-10-17 17:27:47', '2017-10-17 17:27:47', null);
INSERT INTO `ni_product_adjustment` VALUES ('15', '5', '17', '-1', '8', '18', '10000', '0', '10000', 'sale', '2017-10-17 17:27:47', '2017-10-17 17:27:47', null);
INSERT INTO `ni_product_adjustment` VALUES ('16', '5', '18', '-1', '1998', '3998', '30000', '0', '30000', 'sale', '2017-10-17 17:27:47', '2017-10-17 17:27:47', null);
INSERT INTO `ni_product_adjustment` VALUES ('17', '5', '18', '-1978', '20', '2020', '30000', '0', '30000', 'shift report', '2017-10-17 17:31:33', '2017-10-17 17:31:33', null);
INSERT INTO `ni_product_adjustment` VALUES ('18', '5', '16', '40', '50', '110', '10000', '0', '10000', 'shift report', '2017-10-17 17:31:33', '2017-10-17 17:31:33', null);
INSERT INTO `ni_product_adjustment` VALUES ('19', '5', '18', '-2', '18', '2018', '30000', '0', '30000', 'sale', '2017-10-18 10:36:13', '2017-10-18 10:36:13', null);
INSERT INTO `ni_product_adjustment` VALUES ('20', '5', '17', '-2', '6', '16', '10000', '0', '10000', 'sale', '2017-10-18 10:36:13', '2017-10-18 10:36:13', null);
INSERT INTO `ni_product_adjustment` VALUES ('21', '5', '18', '82', '100', '2100', '30000', '0', '30000', 'shift report', '2017-10-18 10:39:25', '2017-10-18 10:39:25', null);
INSERT INTO `ni_product_adjustment` VALUES ('22', '5', '16', '100', '150', '210', '10000', '0', '10000', 'shift report', '2017-10-18 10:39:25', '2017-10-18 10:39:25', null);
INSERT INTO `ni_product_adjustment` VALUES ('23', '5', '18', '-50', '50', '2050', '30000', '0', '30000', 'shift report', '2017-10-18 10:47:12', '2017-10-18 10:47:12', null);
INSERT INTO `ni_product_adjustment` VALUES ('24', '5', '16', '-75', '75', '135', '10000', '0', '10000', 'shift report', '2017-10-18 10:47:12', '2017-10-18 10:47:12', null);
INSERT INTO `ni_product_adjustment` VALUES ('25', '5', '18', '-1', '49', '2049', '30000', '0', '30000', 'sale', '2017-10-18 13:30:56', '2017-10-18 13:30:56', null);
INSERT INTO `ni_product_adjustment` VALUES ('26', '5', '18', '-4', '45', '2045', '30000', '0', '30000', 'shift report', '2017-10-18 13:31:40', '2017-10-18 13:31:40', null);
INSERT INTO `ni_product_adjustment` VALUES ('27', '5', '16', '-10', '65', '125', '10000', '0', '10000', 'shift report', '2017-10-18 13:31:40', '2017-10-18 13:31:40', null);
INSERT INTO `ni_product_adjustment` VALUES ('28', '4', '16', '-1', '9', '124', '10000', '0', '10000', 'sale', '2017-10-19 09:34:43', '2017-10-19 09:34:43', null);
INSERT INTO `ni_product_adjustment` VALUES ('29', '4', '18', '-1', '1999', '2044', '30000', '0', '30000', 'sale', '2017-10-19 09:34:43', '2017-10-19 09:34:43', null);
INSERT INTO `ni_product_adjustment` VALUES ('30', '4', '17', '-1', '9', '15', '10000', '0', '10000', 'sale', '2017-10-19 09:34:43', '2017-10-19 09:34:43', null);
INSERT INTO `ni_product_adjustment` VALUES ('31', '4', '17', '-4', '5', '11', '10000', '0', '10000', 'sale', '2017-10-19 09:36:09', '2017-10-19 09:36:09', null);
INSERT INTO `ni_product_adjustment` VALUES ('32', '4', '17', '-2', '3', '9', '10000', '0', '10000', 'sale', '2017-10-19 09:45:04', '2017-10-19 09:45:04', null);
INSERT INTO `ni_product_adjustment` VALUES ('33', '4', '17', '-5', '-2', '4', '10000', '0', '10000', 'sale', '2017-10-19 09:47:17', '2017-10-19 09:47:17', null);
INSERT INTO `ni_product_adjustment` VALUES ('34', '4', '16', '-1', '8', '123', '10000', '0', '10000', 'sale', '2017-10-19 09:47:17', '2017-10-19 09:47:17', null);
INSERT INTO `ni_product_adjustment` VALUES ('35', '4', '17', '-6', '-8', '-2', '10000', '0', '10000', 'sale', '2017-10-19 09:47:17', '2017-10-19 09:47:17', null);
INSERT INTO `ni_product_adjustment` VALUES ('36', '4', '17', '-4', '-12', '-6', '10000', '0', '10000', 'sale', '2017-10-19 09:47:18', '2017-10-19 09:47:18', null);

-- ----------------------------
-- Table structure for `ni_province`
-- ----------------------------
DROP TABLE IF EXISTS `ni_province`;
CREATE TABLE `ni_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_province
-- ----------------------------
INSERT INTO `ni_province` VALUES ('1', '1', 'Bali', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('2', '2', 'Bangka Belitung', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('3', '3', 'Banten', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('4', '4', 'Bengkulu', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('5', '5', 'DI Yogyakarta', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('6', '6', 'DKI Jakarta', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('7', '7', 'Gorontalo', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('8', '8', 'Jambi', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('9', '9', 'Jawa Barat', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('10', '10', 'Jawa Tengah', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('11', '11', 'Jawa Timur', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('12', '12', 'Kalimantan Barat', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('13', '13', 'Kalimantan Selatan', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('14', '14', 'Kalimantan Tengah', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('15', '15', 'Kalimantan Timur', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('16', '16', 'Kalimantan Utara', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('17', '17', 'Kepulauan Riau', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('18', '18', 'Lampung', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('19', '19', 'Maluku', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('20', '20', 'Maluku Utara', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('21', '21', 'Nanggroe Aceh Darussalam (NAD)', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('22', '22', 'Nusa Tenggara Barat (NTB)', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('23', '23', 'Nusa Tenggara Timur (NTT)', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('24', '24', 'Papua', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('25', '25', 'Papua Barat', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('26', '26', 'Riau', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('27', '27', 'Sulawesi Barat', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('28', '28', 'Sulawesi Selatan', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('29', '29', 'Sulawesi Tengah', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('30', '30', 'Sulawesi Tenggara', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('31', '31', 'Sulawesi Utara', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('32', '32', 'Sumatera Barat', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('33', '33', 'Sumatera Selatan', '2016-06-02 10:59:30', '0000-00-00 00:00:00');
INSERT INTO `ni_province` VALUES ('34', '34', 'Sumatera Utara', '2016-06-02 10:59:30', '0000-00-00 00:00:00');

-- ----------------------------
-- Table structure for `ni_push_notification`
-- ----------------------------
DROP TABLE IF EXISTS `ni_push_notification`;
CREATE TABLE `ni_push_notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `sender` varchar(2) DEFAULT NULL,
  `token` text,
  `platform` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_push_notification
-- ----------------------------
INSERT INTO `ni_push_notification` VALUES ('1', '2', '1', 'cgvsyyQYNjw:APA91bHA3oFl8ERtXz_Y7zirw8RqdDOdj8NSV-C3Ilb6HhXSqliAfeJk5Cgo3C7nNujXYXD7fPzi4xRNZioEx1cf-dKhofqNYj8433NqAl3P3snm1DA05WpnG8m41KWAVKwSJihGCtOg', 'Android');
INSERT INTO `ni_push_notification` VALUES ('2', '4', '2', 'd2MfcePX-xM:APA91bGfGbrdVvwUvRu5etofkakC5Nv9gdmJRBNFvgGESDMLXNqg5VgSPZhOLKtQBB0Q_vxvA8KWoGvne-mWZ8ULrZ5NltsD4_BaACEeJcsbNuMuYbkiqE8R9j8JRtClntTPmYlnoU2l', 'Android');
INSERT INTO `ni_push_notification` VALUES ('3', '2', '1', 'dx9WHGwalOM:APA91bFgWHYrm2DEbvzTzKMclI1mjsuXZy9pE-ULa_YEWybruOgELihwlbmLMst4pSsmv1wQrQ5EANN5pSnRQbIC1JzzuYfukaJqbadEH6JmK-XHuyUs-dS5nb1Fd4i7EhSQJmhlrHTH', 'Android');
INSERT INTO `ni_push_notification` VALUES ('4', '2', '1', 'dBJmET9f2j8:APA91bGDxtZMGtwRwziq6RaI6YbdFnZk2Dh8mQRpdNXqVVyheZZBIHe_3vo3cieUp7MrdBvWjWh_X5F_Ap0v6LMg_ooO-qMGWjP7lE7-3WHAmg_wkE1uv9fvS9HZbI4XN78JQUOjgxfh', 'Android');
INSERT INTO `ni_push_notification` VALUES ('5', '4', '2', 'clKQwAMB8X4:APA91bGTOvj7incVyDUCnLWrtKdWWgIy_md9isa9Psk-zpHzpJ0L7jWZ6s8zkMSweCtziadkmPSKR0uHNuJ63S6_CGEZ7-ql5M_P9galU9fq18bkylWjC9IaP6fU15zuhunh1xFbyjQg', 'Android');
INSERT INTO `ni_push_notification` VALUES ('6', '4', '2', 'fm_5elVHRmQ:APA91bHeXpDxvcSx6kdD9-wdDnKt-JY5wllk7mPfAF1LgXACWAFXxTnCaVbN3arA00-tOGVdgDXR3u_Ltre84Bg1Ou-anIZrmc4w7HSrclYnMWUXmsait9gdzDon5w0aI_4QHXaNbLQ6', 'Android');
INSERT INTO `ni_push_notification` VALUES ('7', '2', '1', 'dU9ZfgZU5Ug:APA91bHrm0oo3TMT9cv7YvHQOpsF_LKWbA-1euVxEbrMKOgwVvWE2dYJ1I4I9RWn--3FJUcnfBmTWByVYFX6BfvpqxNawMTJww9yYU43CN8Z1TxQV2uc6pjk9hAblX5HHcPgJUhpv_uN', 'Android');
INSERT INTO `ni_push_notification` VALUES ('8', '2', '1', 'cJAXruHa6RY:APA91bGs-VPQD9GRN6lEDYbEpRKOEqu4JJTFDAjuJIWKqWCTYyd9lgguhAgCk7pG0D_Z-eYyXtVS8DkTfWUISfrV8Sfso2OOdb-LM03TXLmKXMfo202ldzBiLyPfdEDdmv4MoEpPNCBW', 'Android');

-- ----------------------------
-- Table structure for `ni_service_product`
-- ----------------------------
DROP TABLE IF EXISTS `ni_service_product`;
CREATE TABLE `ni_service_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(100) DEFAULT NULL,
  `service_category` varchar(100) DEFAULT NULL,
  `price` varchar(100) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_service_product
-- ----------------------------
INSERT INTO `ni_service_product` VALUES ('20', 'Tambal ban motor', 'Motor', '4000', 'y', '2017-10-10 11:58:40', '2017-10-10 12:00:18', '1');

-- ----------------------------
-- Table structure for `ni_service_product_dt`
-- ----------------------------
DROP TABLE IF EXISTS `ni_service_product_dt`;
CREATE TABLE `ni_service_product_dt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_product_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `qty_default` int(11) DEFAULT NULL COMMENT 'pcs, psi',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_service_product_dt
-- ----------------------------
INSERT INTO `ni_service_product_dt` VALUES ('2', '20', '16', '1', '2017-10-10 11:58:40', '2017-10-10 11:58:40', '1');
INSERT INTO `ni_service_product_dt` VALUES ('3', '20', '16', '1', '2017-10-10 11:58:40', '2017-10-10 11:58:40', null);

-- ----------------------------
-- Table structure for `ni_shift`
-- ----------------------------
DROP TABLE IF EXISTS `ni_shift`;
CREATE TABLE `ni_shift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `shift_name` varchar(100) DEFAULT NULL,
  `descrption` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `upd_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_shift
-- ----------------------------
INSERT INTO `ni_shift` VALUES ('1', '1', '07:00 - 13:00', '2016-06-02 10:59:30', '2017-10-06 16:05:52', null);
INSERT INTO `ni_shift` VALUES ('2', '2', '13:15 - 19:00', '2016-06-02 10:59:30', '2017-10-06 16:06:09', null);

-- ----------------------------
-- Table structure for `ni_sub_district`
-- ----------------------------
DROP TABLE IF EXISTS `ni_sub_district`;
CREATE TABLE `ni_sub_district` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(10) DEFAULT NULL,
  `sub_district_id` int(10) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6995 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_sub_district
-- ----------------------------

-- ----------------------------
-- Table structure for `ni_transaction_queue`
-- ----------------------------
DROP TABLE IF EXISTS `ni_transaction_queue`;
CREATE TABLE `ni_transaction_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `user_absence_id` int(11) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `transaction_date` timestamp NULL DEFAULT NULL,
  `total_price` int(20) DEFAULT NULL,
  `transaction_dt` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `approve` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_transaction_queue
-- ----------------------------
INSERT INTO `ni_transaction_queue` VALUES ('1', '2', '20', '4', '2017-10-19 09:39:04', '110000', '[{\"id\":17,\"product_name\":\"Karet Cacing\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-10-17 17:13:50\",\"upd_by\":null,\"price\":10000,\"images\":null,\"product_code\":\"KC002\",\"type\":\"Produk\",\"qty\":11,\"detail\":[]}]', '2017-10-19 09:39:04', '2017-10-19 09:39:04', 'n');
INSERT INTO `ni_transaction_queue` VALUES ('2', '2', '20', '4', '2017-10-19 09:40:57', '120000', '[{\"id\":17,\"product_name\":\"Karet Cacing\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-10-17 17:13:50\",\"upd_by\":null,\"price\":10000,\"images\":null,\"product_code\":\"KC002\",\"type\":\"Produk\",\"qty\":12,\"detail\":[]}]', '2017-10-19 09:40:57', '2017-10-19 09:40:57', 'n');
INSERT INTO `ni_transaction_queue` VALUES ('3', '2', '20', '4', '2017-10-19 09:44:07', '120000', '[{\"id\":17,\"product_name\":\"Karet Cacing\",\"unit\":\"Pcs\",\"description\":\"Karet Cacing\",\"status\":\"y\",\"created_at\":\"2017-10-10 17:05:28\",\"updated_at\":\"2017-10-17 17:13:50\",\"upd_by\":null,\"price\":10000,\"images\":null,\"product_code\":\"KC002\",\"type\":\"Produk\",\"qty\":12,\"detail\":[]}]', '2017-10-19 09:44:07', '2017-10-19 09:44:07', 'n');

-- ----------------------------
-- Table structure for `ni_transactiondt`
-- ----------------------------
DROP TABLE IF EXISTS `ni_transactiondt`;
CREATE TABLE `ni_transactiondt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transactionhd_id` int(11) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL,
  `product_name` varchar(50) DEFAULT NULL,
  `detail` longtext,
  `type` varchar(20) DEFAULT NULL COMMENT 'type jasa: product_id relasi ke tabel service_product\r\ntype product: product_id relasi ke tabel product',
  `price` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_transactiondt
-- ----------------------------
INSERT INTO `ni_transactiondt` VALUES ('1', '1', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `ni_transactiondt` VALUES ('2', '1', '17', 'Karet Cacing', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `ni_transactiondt` VALUES ('3', '1', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `ni_transactiondt` VALUES ('4', '2', '17', 'Karet Cacing', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `ni_transactiondt` VALUES ('5', '2', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `ni_transactiondt` VALUES ('6', '3', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":2},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":2}]', 'Jasa', '4000', '2', '2017-10-18 10:36:13', '2017-10-18 10:36:13');
INSERT INTO `ni_transactiondt` VALUES ('7', '3', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '2', '2017-10-18 10:36:13', '2017-10-18 10:36:13');
INSERT INTO `ni_transactiondt` VALUES ('8', '3', '17', 'Karet Cacing', '[]', 'Produk', '10000', '2', '2017-10-18 10:36:13', '2017-10-18 10:36:13');
INSERT INTO `ni_transactiondt` VALUES ('9', '4', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-18 10:39:42', '2017-10-18 10:39:42');
INSERT INTO `ni_transactiondt` VALUES ('10', '5', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-18 10:42:27', '2017-10-18 10:42:27');
INSERT INTO `ni_transactiondt` VALUES ('11', '6', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-18 13:30:56', '2017-10-18 13:30:56');
INSERT INTO `ni_transactiondt` VALUES ('12', '6', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-18 13:30:56', '2017-10-18 13:30:56');
INSERT INTO `ni_transactiondt` VALUES ('13', '7', '16', 'karet cacing 2', '[]', 'Produk', '10000', '1', '2017-10-19 09:34:43', '2017-10-19 09:34:43');
INSERT INTO `ni_transactiondt` VALUES ('14', '7', '18', 'Tabung Nitrogen', '[]', 'Produk', '10000', '1', '2017-10-19 09:34:43', '2017-10-19 09:34:43');
INSERT INTO `ni_transactiondt` VALUES ('15', '7', '17', 'Karet Cacing', '[]', 'Produk', '10000', '1', '2017-10-19 09:34:43', '2017-10-19 09:34:43');
INSERT INTO `ni_transactiondt` VALUES ('16', '8', '17', 'Karet Cacing', '[]', 'Produk', '10000', '4', '2017-10-19 09:36:09', '2017-10-19 09:36:09');
INSERT INTO `ni_transactiondt` VALUES ('17', '9', '17', 'Karet Cacing', '[]', 'Produk', '10000', '2', '2017-10-19 09:45:04', '2017-10-19 09:45:04');
INSERT INTO `ni_transactiondt` VALUES ('18', '10', '17', 'Karet Cacing', '[]', 'Produk', '10000', '5', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `ni_transactiondt` VALUES ('19', '10', '20', 'Tambal ban motor', '[{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1},{\"id\":16,\"product_name\":\"karet cacing 2\",\"unit\":\"Psi\",\"description\":\"abc\",\"status\":\"y\",\"created_at\":\"2017-09-28 06:35:20\",\"updated_at\":\"2017-10-16 10:45:08\",\"upd_by\":\"1\",\"price\":10000,\"images\":\"lzZWJT_Hydrangeas.jpg\",\"product_code\":\"KC001\",\"qty_default\":1}]', 'Jasa', '4000', '1', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `ni_transactiondt` VALUES ('20', '10', '16', 'karet cacing 2', '[]', 'Produk', '10000', '1', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `ni_transactiondt` VALUES ('21', '11', '17', 'Karet Cacing', '[]', 'Produk', '10000', '6', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `ni_transactiondt` VALUES ('22', '12', '17', 'Karet Cacing', '[]', 'Produk', '10000', '4', '2017-10-19 09:47:18', '2017-10-19 09:47:18');

-- ----------------------------
-- Table structure for `ni_transactionhd`
-- ----------------------------
DROP TABLE IF EXISTS `ni_transactionhd`;
CREATE TABLE `ni_transactionhd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_id` varchar(20) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `user_absence_id` int(11) DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `transaction_date` timestamp NULL DEFAULT NULL,
  `total_price` int(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_transactionhd
-- ----------------------------
INSERT INTO `ni_transactionhd` VALUES ('1', '17101700000001', '2', '16', '5', '2017-10-17 17:27:47', '24000', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `ni_transactionhd` VALUES ('2', '17101700000002', '2', '16', '5', '2017-10-17 17:27:47', '20000', '2017-10-17 17:27:47', '2017-10-17 17:27:47');
INSERT INTO `ni_transactionhd` VALUES ('3', '18101700000001', '2', '17', '5', '2017-10-18 10:36:13', '48000', '2017-10-18 10:36:13', '2017-10-18 10:36:13');
INSERT INTO `ni_transactionhd` VALUES ('4', '18101700000002', '2', '17', '5', '2017-10-18 10:39:42', '4000', '2017-10-18 10:39:42', '2017-10-18 10:39:42');
INSERT INTO `ni_transactionhd` VALUES ('5', '18101700000003', '2', '17', '5', '2017-10-18 10:42:27', '4000', '2017-10-18 10:42:27', '2017-10-18 10:42:27');
INSERT INTO `ni_transactionhd` VALUES ('6', '18101700000004', '2', '19', '5', '2017-10-18 13:30:56', '14000', '2017-10-18 13:30:56', '2017-10-18 13:30:56');
INSERT INTO `ni_transactionhd` VALUES ('7', '19101700000001', '2', '20', '4', '2017-10-19 09:34:43', '30000', '2017-10-19 09:34:43', '2017-10-19 09:34:43');
INSERT INTO `ni_transactionhd` VALUES ('8', '19101700000002', '2', '20', '4', '2017-10-19 09:36:09', '40000', '2017-10-19 09:36:09', '2017-10-19 09:36:09');
INSERT INTO `ni_transactionhd` VALUES ('9', '19101700000003', '2', '20', '4', '2017-10-19 09:45:04', '20000', '2017-10-19 09:45:04', '2017-10-19 09:45:04');
INSERT INTO `ni_transactionhd` VALUES ('10', '19101700000004', '2', '20', '4', '2017-10-19 09:47:17', '64000', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `ni_transactionhd` VALUES ('11', '19101700000005', '2', '20', '4', '2017-10-19 09:47:17', '60000', '2017-10-19 09:47:17', '2017-10-19 09:47:17');
INSERT INTO `ni_transactionhd` VALUES ('12', '19101700000006', '2', '20', '4', '2017-10-19 09:47:18', '40000', '2017-10-19 09:47:18', '2017-10-19 09:47:18');

-- ----------------------------
-- Table structure for `ni_user`
-- ----------------------------
DROP TABLE IF EXISTS `ni_user`;
CREATE TABLE `ni_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `user_access_id` int(11) DEFAULT NULL,
  `language_id` tinyint(2) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `images` varchar(100) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `last_activity` varchar(100) DEFAULT NULL,
  `description` text,
  `remember_token` varchar(100) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `upd_by` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `login_app` varchar(1) DEFAULT NULL,
  `login_backend` varchar(1) DEFAULT NULL,
  `outlet_id` text,
  `token` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_user
-- ----------------------------
INSERT INTO `ni_user` VALUES ('1', 'admin', '1', '2', 'admin@admin.com', '$2y$10$pGWG4e/C/hjEKj2QRFDWxOLdcxvfE2PCWCTaV8tjhr0TZyyBMDqQ.', 'vincent', 'GhsSde_2-9670.jpg', null, null, 'cent', 'rvYwvKKN6opqH2VaeIXLKHCEdnwwczFa43ShekFIRvkmXq1EVoO2czjAT49T', 'y', '1', '2015-09-30 02:02:34', '2017-09-27 09:17:25', null, null, null, null);
INSERT INTO `ni_user` VALUES ('2', 'op_nitro', '3', '2', null, '$2a$10$E/jjRSKlocCRkFAW7XqEY.NDggZycguGT2JhG6rfYmRY36CaU9DhO', 'Operator 1', null, null, '2017-10-19 10:13:25', null, null, 'y', null, '2017-10-02 10:37:50', '2017-10-19 10:13:25', 'y', 'n', ';4;5;6;', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJrYW1wcmV0IiwicGFzc3dvcmQiOiIkMmEkMTAkRS9qalJTS2xvY0NSa0ZBVzdYcUVZLk5EZ2daeWNndUdUMkpoRzZyZlltUlkzNkNhVTlEaE8iLCJpbWFnZXMiOm51bGwsIm5hbWUiOiJCYW5na2UiLCJzdGF0dXMiOiJ5IiwiaWF0IjoxNTA4MzgxMTUxLCJleHAiOjE1MTYxNTcxNTF9.OmDBF9LV2ziv-l5BAo4z_NiQNcBpgAMNkMliQFPuZFk');
INSERT INTO `ni_user` VALUES ('3', 'admin_nitro', '2', '2', null, '$2a$10$OMEV4/puErorlDRmJgILHeKMEKsxzpQKE6k5GWszuRKNZizmQ.u.a', 'Admin Nitrogen', null, null, '2017-10-19 09:41:31', null, null, 'y', null, '0000-00-00 00:00:00', '2017-10-19 09:49:36', 'y', 'n', '0', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MywidXNlcm5hbWUiOiJiZWdvIiwicGFzc3dvcmQiOiIkMmEkMTAkT01FVjQvcHVFcm9ybERSbUpnSUxIZUtNRUtzeHpwUUtFNms1R1dzenVSS05aaXptUS51LmEiLCJpbWFnZXMiOm51bGwsIm5hbWUiOiJUb2xvbCIsInN0YXR1cyI6InkiLCJpYXQiOjE1MDgzMDc4NjMsImV4cCI6MTUxNjA4Mzg2M30.mSJk5rD_CmdHnuGDHgRLyPfBGURPFhNHV8M9qfFw4RY');

-- ----------------------------
-- Table structure for `ni_user_absence`
-- ----------------------------
DROP TABLE IF EXISTS `ni_user_absence`;
CREATE TABLE `ni_user_absence` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(15) DEFAULT NULL,
  `clock_in` timestamp NULL DEFAULT NULL,
  `clock_out` timestamp NULL DEFAULT NULL,
  `outlet_id` int(11) DEFAULT NULL,
  `shift_id` int(11) DEFAULT NULL,
  `description` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_user_absence
-- ----------------------------
INSERT INTO `ni_user_absence` VALUES ('1', '2', '2017-10-10 13:45:41', '2017-10-18 13:31:40', '2', '1', 'done', null, null);
INSERT INTO `ni_user_absence` VALUES ('7', '2', '2017-10-12 14:10:28', '2017-10-18 13:31:40', '5', '1', 'done', null, null);
INSERT INTO `ni_user_absence` VALUES ('8', '2', '2017-10-15 20:06:12', '2017-10-18 13:31:40', '5', '1', 'done', null, null);
INSERT INTO `ni_user_absence` VALUES ('13', '2', '2017-10-16 20:37:27', '2017-10-18 13:31:40', '5', '1', 'done', null, null);
INSERT INTO `ni_user_absence` VALUES ('16', '2', '2017-10-17 16:38:13', '2017-10-18 13:31:40', '5', '1', 'done', null, null);
INSERT INTO `ni_user_absence` VALUES ('19', '2', '2017-10-18 13:28:57', '2017-10-18 13:31:40', '5', '1', 'done', null, null);
INSERT INTO `ni_user_absence` VALUES ('20', '2', '2017-10-19 09:33:02', null, '4', '1', null, null, null);

-- ----------------------------
-- Table structure for `ni_useraccess`
-- ----------------------------
DROP TABLE IF EXISTS `ni_useraccess`;
CREATE TABLE `ni_useraccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_name` varchar(40) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ni_useraccess
-- ----------------------------
INSERT INTO `ni_useraccess` VALUES ('1', 'Super Admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
