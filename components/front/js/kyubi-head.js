$(document).ready(function(){
  //Config
  $( document ).ajaxStart(function() {
    $( "#overlay" ).show();
  });
   $( document ).ajaxStop(function() {
    $( "#overlay" ).hide();  
  });
  $.ajaxSetup({
    beforeSend: function( xhr ) {
       xhr.setRequestHeader('X-CSRF-Token', $('meta[name="csrf-token"]').attr('content'))
    }
  });

  //Function / Helper
  $.extend({
  	root:function(){
  		return $('meta[name="root_url"]').attr('content');
  	},postformdata:function(url,formdata){
      data = $.ajax({
              url: url,
              type: "POST",
              data: formdata,
              contentType: false, 
              cache: false,
              processData:false,
          });
          return data;
    },postdata:function(url,formdata){
      data = $.ajax({
              url: url,
              type: "POST",
              data: formdata
          });
          return data;
    },getdata:function(url){
      data = $.ajax({
              url: url,
              type: "GET"
          });
          return data;
    },validemail:function(email){
      var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
          return pattern.test(email);
    },settimeoutalert:function(){
      window.setTimeout(function() {
          $(".alert alert-remove").fadeTo(1500, 0).slideUp(500, function(){
            $(this).remove(); 
          });
      }, 2000);
    },growl_alert:function(text,type){
      $.bootstrapGrowl(text, {
        ele: 'body', // which element to append to
        type: type, // (null, 'info', 'danger', 'success', 'warning')
        offset: {
            from: 'top',
            amount: 100
        }, // 'top', or 'bottom'
        align: 'right', // ('left', 'right', or 'center')
        width: 250, // (integer, or 'auto')
        delay: 5000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
        allow_dismiss: true, // If true then will display a cross to close the popup.
        stackup_spacing: 10 // spacing between consecutively stacked growls.
      });
      $.settimeoutalert();
    }
  });

  //Core Jquery
  var miniCartContainer   = $('.open-cart-popup');
  var miniCart            = $('.cart-box > .popup-container > .mini-cart');
  var totalCart           = $('.total-cart');
  var cartParts           = $('.cart-parts');
  var needLoad            = 0;
  $.extend({
    coreCart:function(){
      $(document).on('click','.add-cart',function(){
        that            = this;
        var productAttr = $('.product-attr.active');
        var productQty  = $('.product-qty');
        var productId   = $(that).data('id');
        var qty         = productQty.text();
        var attr        = [];

        productAttr.each(function(i,v){
          var attrId  = $(this).data('id');
          attr.push(attrId);
        });
        var data        = {'id':productId,'attr':attr,'qty':qty};
        $(that).attr('disabled',true);
        $.postdata($.root()+'v1/add-cart',data).success(function(res){
          if(res.result.status == 'available'){
            $.emptyMiniCart();
            $.setTotalCart();
            $("html, body").animate({ scrollTop: 0 }, 600);
            setTimeout(function(){
              $(miniCartContainer).trigger('mouseover');
            },1000);
          }
          $.growl_alert(res.result.text,res.result.type);
          $(that).attr('disabled',false);
        });
      });

      $(document).on('click','.remove-mini-cart',function(){
        var id    = $(this).data('id');
        var data  = {'id' : id};
        $.postdata($.root()+'v1/remove-cart',data).success(function(res){
          $.emptyMiniCart();
          $.setTotalCart();
        });
      });

      $(document).on('click','.add-wishlist',function(){
        $.growl_alert('Product added to wishlist','success');
      });

      if($('.cart-box').length > 0){        
        $(document).on('mouseover',miniCartContainer,function(){
          if($(miniCart).is(':empty')){
            needLoad  += 1;
          }
          if(needLoad == 1){
            $('.mini-cart-loader').show();
            setTimeout(function(){
              $.miniCart();
            },1000);
          }
        });
      }
    },miniCart:function(){
      $.getdata($.root()+'parts/mini-cart').success(function(res){
        $(miniCart).html(res);
        needLoad  = 0;
        $('.mini-cart-loader').hide();
      }).error(function(err){
        needLoad  = 0;
      });
    },emptyMiniCart:function(){
      $(miniCart).html('');
      needLoad  = 0;
    },setTotalCart:function(){
      $.getdata($.root()+'v1/total-cart').success(function(res){
        $(totalCart).html(res);
        $.miniCart();
      });
    },cartParts:function(){
      $.getdata($.root()+'parts/cart').success(function(res){
        cartParts.html(res);
        $('.cart-loader').fadeOut();
      });
    }
  })
  $.coreCart();
  setTimeout(function(){
    $.setTotalCart();
    if($(cartParts).length > 0){
      $.cartParts();
    }
  },1500);
});